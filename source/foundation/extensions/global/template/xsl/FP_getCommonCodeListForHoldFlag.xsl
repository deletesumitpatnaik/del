<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
	<CommonCode>
	<xsl:attribute name="CodeType">
        <xsl:value-of select="'BuyerRemorseHoldFlag'"/>
    </xsl:attribute>
	<xsl:attribute name="CodeValue">
        <xsl:value-of select="Order/@SellerOrganizationCode"/>
    </xsl:attribute>
	<xsl:attribute name="DocumentType">
        <xsl:value-of select="'0001'"/>
    </xsl:attribute>
	</CommonCode>
	</xsl:template>
</xsl:stylesheet>