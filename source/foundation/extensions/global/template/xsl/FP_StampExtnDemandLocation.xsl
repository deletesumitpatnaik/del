<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:template match="/">


<xsl:element name="Order">
<xsl:copy-of select="Order/@*"/>
<xsl:copy-of select="Order/*[name()!='OrderLines' and name()!='Promise']"/>
<xsl:element name="OrderLines">
<xsl:for-each select="Order/OrderLines/OrderLine">

<xsl:variable name="vExtnLineID">
<xsl:value-of select="Extn/@ExtnLineID"/>
</xsl:variable>
<xsl:element name="OrderLine">
<xsl:copy-of select="/Order/OrderLines/OrderLine/*[name()!='Extn']" />
<xsl:for-each select="..//..//./PromiseLines/PromiseLine">
<xsl:variable name="vLineId">
<xsl:value-of select="..//..//./PromiseLines/PromiseLine/@LineId"/>
</xsl:variable>
<xsl:element name="Extn">
<xsl:copy-of select="/Order/OrderLines/OrderLine/Extn/@*" />
<xsl:variable name="vExtnDemandLocation">
<xsl:if test="$vLineId = $vExtnLineID">
<xsl:value-of select="..//..//./PromiseLines/PromiseLine/@ShipNode"></xsl:value-of>
</xsl:if> 
</xsl:variable>
<xsl:attribute name="ExtnDemandLocation">
<xsl:value-of select="$vExtnDemandLocation"/>
</xsl:attribute>
</xsl:element>	
</xsl:for-each>
</xsl:element>
</xsl:for-each>
</xsl:element>
</xsl:element>
</xsl:template>
</xsl:stylesheet>