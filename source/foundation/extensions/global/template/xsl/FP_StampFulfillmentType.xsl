<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
        <xsl:variable name="vSellerOrganizationCode">
                <xsl:value-of select="/Order/@SellerOrganizationCode" />
       </xsl:variable>
	   <xsl:variable name="vDeliveryMethod">
                <xsl:value-of select="/Order/OrderLines/OrderLine/@DeliveryMethod" />
       </xsl:variable>
	   <xsl:variable name="vState">
               <xsl:value-of select="/Order/OrderLines/OrderLine/PersonInfoShipTo/@State"/>
        </xsl:variable>
		<xsl:variable name="vExtnDcSwitch">
               <xsl:value-of select="/Order/OrderLines/OrderLine/Extn/@ExtnDcSwitch"/>
       </xsl:variable>
       <xsl:element name="Order">
	   <xsl:copy-of select="/Order/@*" />
       <xsl:copy-of select="Order/*[name()!='OrderLines']"/>
       <xsl:element name="OrderLines">
	   <xsl:element name="OrderLine">
       <xsl:copy-of select="/Order/OrderLines/OrderLine/@*" />
       <xsl:attribute name="FulfillmentType">
	   <xsl:if test="($vSellerOrganizationCode='FleetPride') and (($vExtnDcSwitch = 'Y' or $vExtnDcSwitch = '' or $vExtnDcSwitch='null' ) and $vDeliveryMethod='SHP')">
       <xsl:value-of select="'FT_Y'" />
       </xsl:if>
	   <xsl:if test="($vSellerOrganizationCode='FleetPride') and (($vExtnDcSwitch = 'N' ) and $vDeliveryMethod='SHP')">
       <xsl:value-of select="'FT'" />
       </xsl:if>
	   <xsl:if test="($vSellerOrganizationCode!='FleetPride') and ($vDeliveryMethod='SHP')">
       <xsl:value-of select="'FT'" />
       </xsl:if>
       <xsl:if test="($vDeliveryMethod='PICK')or($vDeliveryMethod='DEL')">
       <xsl:value-of select="concat('FT_',$vState)"/>
       </xsl:if>
       </xsl:attribute>
	   <xsl:copy-of select="/Order/OrderLines/OrderLine/*" />
	   </xsl:element>
       </xsl:element>
       </xsl:element>
	</xsl:template>
</xsl:stylesheet>