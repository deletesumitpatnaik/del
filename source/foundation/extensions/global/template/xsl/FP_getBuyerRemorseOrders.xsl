<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xalan/java">
<xsl:output indent="yes" />
	<xsl:template match="/">
		
			<Order>  
				<xsl:variable name="dateTimeFormat" select="&quot;yyyy-MM-dd'T'HH:mm:ss&quot;"  />
				<xsl:variable name="varNoOfMins" select="/MessageXml/@NumberOfMinutes"  />
				<xsl:variable name="OrderHeaderKey" select="/MessageXml/LastMessage/Order/@OrderHeaderKey" /> 
				<xsl:variable name="varDate" select="java:com.infosys.es.scm.coe.util.DateUtil.getPreviousTime($dateTimeFormat, $varNoOfMins)"/> 
			
				<xsl:attribute name="MaximumRecords" >
					<xsl:value-of select="/MessageXml/@NumRecordsToBuffer"/>
				</xsl:attribute >
				<xsl:attribute name="EnterpriseCode" >
					<xsl:value-of select="MessageXml/LastMessage/Order/@EnterpriseCode"/>
				</xsl:attribute >
				<xsl:attribute name="Createts">
						<xsl:value-of select="$varDate"/>
					</xsl:attribute>
					<xsl:attribute name="CreatetsQryType">
						<xsl:value-of select="'LT'"/>
					</xsl:attribute>
				<xsl:if test="$OrderHeaderKey!=''">
					<xsl:attribute name="OrderHeaderKeyQryType">
						<xsl:value-of select="'GT'"/>
					 </xsl:attribute>				
					<xsl:attribute name="OrderHeaderKey">
						<xsl:value-of select="MessageXml/LastMessage/Order/@OrderHeaderKey"/>
					</xsl:attribute>
				</xsl:if>
				
				<OrderHoldType HoldType="BuyerRemorseHold" Status="1100"/>
					
				<xsl:element name="OrderBy">
					<xsl:element name="Attribute">
						<xsl:attribute name="Desc">
							<xsl:value-of select="'N'"/>
						</xsl:attribute>
					
						<xsl:attribute name="Name">
							<xsl:value-of select="'OrderHeaderKey'"/>
						</xsl:attribute>
					</xsl:element>
				</xsl:element>
				
			</Order>
		
    </xsl:template>
</xsl:stylesheet>

