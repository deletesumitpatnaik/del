<!-- File Name: multiapi.xsl

Description: This XSL is to convert request payload coming from Sales force to multiapi input

Modification Log:                
**************************************Initial Draft******************************************** 
Date            Author             Version           Modified Date       Modified by           
2/12/2021       Rucha Chavare       1.0              
3/11/2021      		            1.1                 3/11/2021          Priyanka Pant   Modified condition check on AllocationRuleID and added condition check on ShipToAddress                                         
**************************************Initial Draft******************************************** 
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
      <xsl:element name="MultiApi">
         <xsl:if test="not(/Promise/@AllocationRuleID) or (/Promise/@AllocationRuleID= 'COST')or (/Promise/@AllocationRuleID= '')">
            <xsl:element name="API">
               <xsl:attribute name="FlowName">
                  <xsl:value-of select="'FP_GetFindInventoryWithCostService'" />
               </xsl:attribute>
               <xsl:element name="Input">
                  <xsl:element name="Promise">
                     <xsl:copy-of select="/Promise/@*" />
                     <xsl:element name="ExcludedShipNodes">
                        <xsl:copy-of select="/Promise/ExcludedShipNodes/*" />
                     </xsl:element>
              <xsl:if test="/Promise/ShipToAddress">
                            <xsl:element name="ShipToAddress">
                            <xsl:copy-of select="/Promise/ShipToAddress/@*" />
                            </xsl:element>
                        </xsl:if>
                     <xsl:element name="PromiseLines">
                      <xsl:for-each select="/Promise/PromiseLines/PromiseLine">  
                     <xsl:element name="PromiseLine">
                         <xsl:copy-of select="@*" />
                         <xsl:choose>
                        <xsl:when test="./ShipToAddress">
                            <xsl:element name="ShipToAddress">
                            <xsl:copy-of select="./ShipToAddress/@*" />
                            </xsl:element>
                        </xsl:when>
                    <xsl:otherwise>
                            <xsl:element name="ShipToAddress">
                            <xsl:copy-of select="../../ShipToAddress/@*" />
                            </xsl:element>                                     
                    </xsl:otherwise>
                 </xsl:choose>
                     </xsl:element>
                        </xsl:for-each>
                     </xsl:element>
                  </xsl:element>
               </xsl:element>
            </xsl:element>
            </xsl:if>
            <xsl:if test="not(/Promise/@AllocationRuleID) or (/Promise/@AllocationRuleID= 'PRIORITY')or (/Promise/@AllocationRuleID= '')">
            <xsl:element name="API">
               <xsl:attribute name="FlowName">
                  <xsl:value-of select="'FP_GetFindInventoryWithPriorityService'" />
               </xsl:attribute>
               <xsl:element name="Input">
                  <xsl:element name="Promise">
                     <xsl:copy-of select="/Promise/@*" />
                     <xsl:element name="ExcludedShipNodes">
                        <xsl:copy-of select="/Promise/ExcludedShipNodes/*" />
                     </xsl:element>
                     <xsl:if test="/Promise/ShipToAddress">
                            <xsl:element name="ShipToAddress">
                            <xsl:copy-of select="/Promise/ShipToAddress/@*" />
                            </xsl:element>
                        </xsl:if>
                        <xsl:element name="PromiseLines">
                         <xsl:for-each select="/Promise/PromiseLines/PromiseLine">
                        <xsl:element name="PromiseLine">
                           <xsl:copy-of select="@*" />
                             <xsl:choose>
                        <xsl:when test="./ShipToAddress">
                            <xsl:element name="ShipToAddress">
                            <xsl:copy-of select="./ShipToAddress/@*" />
                            </xsl:element>
                        </xsl:when>
                    <xsl:otherwise>
                            <xsl:element name="ShipToAddress">
                            <xsl:copy-of select="../../ShipToAddress/@*" />
                            </xsl:element>                                     
                    </xsl:otherwise>
                 </xsl:choose>
                            </xsl:element>
                        </xsl:for-each>
                     </xsl:element>
                  </xsl:element>
               </xsl:element>
            </xsl:element>
            </xsl:if>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>



<!--
Sample Input

<?xml version="1.0" encoding="UTF-8"?>
<Promise AllocationRuleID="" CheckCapacity="Y" CheckInventory="Y" IgnorePromised="N" IgnoreUnPromised="N" MaximumRecords="10" OrderTotal="" OrderWeight="" OrganizationCode="FleetPride" EnterpriseCode="FleetPride">
   <ExcludedShipNodes>
      <ExcludedShipNode Node="" SuppressNodeCapacity="" SupressProcurement="" SupressSourcing="" />
   </ExcludedShipNodes>
   <PromiseLines>
      <PromiseLine ShipNode="" DeliveryMethod="SHP" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
         <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="LLA" City="Commerce" Country="US" State="CA" ZipCode="90022" />
      </PromiseLine>
      <PromiseLine ShipNode="" DeliveryMethod="PICK" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
         <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="EME" City="Commerce" Country="US" State="TX" ZipCode="90022" />
      </PromiseLine>
   </PromiseLines>
</Promise>

Sample Output

<?xml version="1.0" encoding="UTF-8"?>
<MultiApi>
    <API FlowName="GetFindInventoryWithCost">
        <Input>
            <Promise AllocationRuleID="" CheckCapacity="Y" CheckInventory="Y" IgnorePromised="N" IgnoreUnPromised="N" MaximumRecords="10" OrderTotal="" OrderWeight="" OrganizationCode="FleetPride" EnterpriseCode="FleetPride" DcSwitch="Y">
                <ExcludedShipNodes>
                    <ExcludedShipNode Node="" SuppressNodeCapacity="" SupressProcurement="" SupressSourcing=""/>
                </ExcludedShipNodes>
                <PromiseLines>
                    <PromiseLine ShipNode="" DeliveryMethod="SHP" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
                        <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="LLA" City="Commerce" Country="US" State="CA" ZipCode="90022"/>
                    </PromiseLine>
                    <PromiseLine ShipNode="" DeliveryMethod="PICK" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
                        <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="EME" City="Commerce" Country="US" State="TX" ZipCode="90022"/>
                    </PromiseLine>
                </PromiseLines>
            </Promise>
        </Input>
    </API>
    <API FlowName="GetFindInventoryWithPriority">
        <Input>
            <Promise AllocationRuleID="" CheckCapacity="Y" CheckInventory="Y" IgnorePromised="N" IgnoreUnPromised="N" MaximumRecords="10" OrderTotal="" OrderWeight="" OrganizationCode="FleetPride" EnterpriseCode="FleetPride" DcSwitch="Y">
                <ExcludedShipNodes>
                    <ExcludedShipNode Node="" SuppressNodeCapacity="" SupressProcurement="" SupressSourcing=""/>
                </ExcludedShipNodes>
                <PromiseLines>
                    <PromiseLine ShipNode="" DeliveryMethod="SHP" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
                        <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="LLA" City="Commerce" Country="US" State="CA" ZipCode="90022"/>
                    </PromiseLine>
                    <PromiseLine ShipNode="" DeliveryMethod="PICK" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
                        <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="EME" City="Commerce" Country="US" State="TX" ZipCode="90022"/>
                    </PromiseLine>
                </PromiseLines>
            </Promise>
        </Input>
    </API>
</MultiApi>

-->