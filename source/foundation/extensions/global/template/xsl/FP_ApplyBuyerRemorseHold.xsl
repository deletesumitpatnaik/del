<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
      <xsl:variable name="vCodeShortDescription">
         <xsl:value-of select="/Order/CommonCodeList/CommonCode/@CodeShortDescription" />
      </xsl:variable>
      <xsl:element name="Order">
         <xsl:copy-of select="/Order/@*" />
         <xsl:copy-of select="Order/*[name()!='CommonCodeList']" />
         <xsl:if test="($vCodeShortDescription='Y')">
            <OrderHoldTypes>
               <OrderHoldType>
                  <xsl:attribute name="HoldType">
                     <xsl:value-of select="'BuyerRemorseHold'" />
                  </xsl:attribute>
                  <xsl:attribute name="ReasonText">
                     <xsl:value-of select="'Applied on Create order'" />
                  </xsl:attribute>
               </OrderHoldType>
            </OrderHoldTypes>
         </xsl:if>
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>