<!-- File Name: findinventorycost.xsl

Description: This XSL is to add custom logic to add fulfilment type based on DcSwitch/DeliveryMethod and Seller Organization.
Also we are setting AllocationRuleID based on COST optimisation  
 
Modification Log:                
**************************************Initial Draft******************************************** 
Date            Author             Version           Modified Date       Modifications           
2/12/2021       Rucha Chavare       1.0                                                   
11/03/2021   	Om Kumar Kaushik	1.1									 Added conditions to check address at line and header level     
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
       <xsl:variable name="vDCSwitch">
                 <xsl:value-of select="/Promise/@DCSwitch" />
       </xsl:variable>
       <xsl:variable name="vOrganizationCode">
                 <xsl:value-of select="/Promise/@OrganizationCode" />
       </xsl:variable>
        <xsl:element name="Promise">
             <xsl:copy-of select="/Promise/@*" />
            <xsl:attribute name="AllocationRuleID">
               <xsl:value-of select="'COST'" />
            </xsl:attribute>
            <xsl:element name="ExcludedShipNodes">
               <xsl:copy-of select="/Promise/ExcludedShipNodes/*" />
            </xsl:element>
            <xsl:element name="PromiseLines">    
             <xsl:for-each select="/Promise/PromiseLines/PromiseLine">
              <xsl:element name="PromiseLine">
                <xsl:choose>
					<xsl:when test="./ShipToAddress"> 
						<xsl:variable name="vState">
						<xsl:value-of select="./ShipToAddress/@State" />
						</xsl:variable>
					</xsl:when>
					<xsl:otherwise>
					        <xsl:variable name="vState">
							<xsl:value-of select="../../ShipToAddress/@State" />
							</xsl:variable>						
					</xsl:otherwise>
				</xsl:choose>
                  <xsl:copy-of select="@*[name()!='FulfillmentType']" />
                  <xsl:attribute name="FulfillmentType">
                     <xsl:choose>
                        <xsl:when test="($vOrganizationCode='FleetPride') and (($vDCSwitch = 'Y' or $vDCSwitch = '' or $vDCSwitch = 'ON') and @DeliveryMethod='SHP')" >
                            <xsl:value-of select="'FT_Y'" />
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:if test="@DeliveryMethod='SHP'">
                              <xsl:value-of select="'FT'" />
                           </xsl:if>
                           <xsl:if test="@DeliveryMethod='PICK'">
                            <xsl:choose>
								<xsl:when test="./ShipToAddress"> 
									<xsl:value-of select="concat('FT_',./ShipToAddress/@State)" />
								</xsl:when>
								<xsl:otherwise>										
										<xsl:value-of select="concat('FT_',../../ShipToAddress/@State)" />
												
								</xsl:otherwise>
								</xsl:choose>
                             <!-- <xsl:value-of select="concat('FT_','$vState')" />-->
                           </xsl:if>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:attribute>
				  <xsl:choose>
						<xsl:when test="./ShipToAddress"> 
							<xsl:element name="ShipToAddress">
							<xsl:copy-of select="./ShipToAddress/@*" />
							</xsl:element>
						</xsl:when>
					<xsl:otherwise>
							<xsl:element name="ShipToAddress">
							<xsl:copy-of select="../../ShipToAddress/@*" />
							</xsl:element>									  
					</xsl:otherwise>
				 </xsl:choose>
               </xsl:element>
             </xsl:for-each>
            </xsl:element>
         </xsl:element>
   </xsl:template>
</xsl:stylesheet>

<!-- Sample Input

<?xml version="1.0" encoding="UTF-8"?>
<Promise AllocationRuleID="" CheckCapacity="Y" CheckInventory="Y" IgnorePromised="N" IgnoreUnPromised="N" MaximumRecords="10" OrderTotal="" OrderWeight="" OrganizationCode="FleetPride" EnterpriseCode="FleetPride" DcSwitch="N">
<ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="LLA" City="Commerce" Country="US" State="WA" ZipCode="70022" />
   <ExcludedShipNodes>
      <ExcludedShipNode Node="sample" SuppressNodeCapacity="Y" SupressProcurement="N" SupressSourcing="" />
   </ExcludedShipNodes>
   <PromiseLines>
      <PromiseLine ShipNode="" DeliveryMethod="SHP" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
         <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="LLA" City="Commerce" Country="US" State="CA" ZipCode="90022" />
      </PromiseLine>
      <PromiseLine ShipNode="" DeliveryMethod="PICK" FulfillmentType="" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH">
         <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="EME" City="Commerce" Country="US" State="TX" ZipCode="80022" />
      </PromiseLine>
   </PromiseLines>
</Promise>

Sample Output

<?xml version="1.0" encoding="UTF-8"?>
<Promise AllocationRuleID="COST" CheckCapacity="Y" CheckInventory="Y" IgnorePromised="N" IgnoreUnPromised="N" MaximumRecords="10" OrderTotal="" OrderWeight="" OrganizationCode="FleetPride" EnterpriseCode="FleetPride" DcSwitch="N">
    <ExcludedShipNodes>
        <ExcludedShipNode Node="sample" SuppressNodeCapacity="Y" SupressProcurement="N" SupressSourcing=""/>
    </ExcludedShipNodes>
    <PromiseLines>
        <PromiseLine ShipNode="" DeliveryMethod="SHP" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH" FulfillmentType="FT">
            <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="LLA" City="Commerce" Country="US" State="CA" ZipCode="90022"/>
        </PromiseLine>
        <PromiseLine ShipNode="" DeliveryMethod="PICK" GroupID="" IsProcurementAllowed="Y" ItemID="Test" LineId="" LineTotal="" LineWeight="" ProcureFromNode="" ProductClass="" RequiredQty="120" UnitOfMeasure="EACH" FulfillmentType="FT_TX">
            <ShipToAddress AdressLine1="abc" AdressLine2="xyz" AdressLine3="EME" City="Commerce" Country="US" State="TX" ZipCode="90022"/>
        </PromiseLine>
    </PromiseLines>
</Promise>

-->
