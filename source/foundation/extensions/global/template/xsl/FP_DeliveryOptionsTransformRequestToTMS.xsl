<!-- File Name: FP_DeliveryOptionsTransformRequestToTMS.xsl

Description: This XSL is to transform and prepare request for TMS call
  
Modification Log:                
**************************************Initial Draft******************************************** 
Date            Author             Version           Modified Date       Modified by           
2/24/2021       Rucha Chavare       1.0                                                        
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
   <xsl:template match="/">
      <xsl:element name="CISDocument">
         <xsl:element name="ApiHeader">
            <xsl:element name="OperationName">
               <xsl:value-of select="'processRateQuotation'" />
            </xsl:element>
         </xsl:element>
         <!--xsl:variable name="vDate">
            <xsl:value-of select=".//@LatestShipDate" />
         </xsl:variable>
         <xsl:for-each select="PromiseShipment"-->
         <xsl:element name="RateCriteria">
            <xsl:element name="ProcessARSideFlag">
               <xsl:value-of select="'false'" />
            </xsl:element>
            <xsl:element name="ShipFromLocationCode">
               <xsl:value-of select=".//ShipFromAddress/@ShipFromLocation" />
            </xsl:element>
            <xsl:element name="ShipToLocationCode">
               <xsl:value-of select=".//ShipToAddress/@ShipToLocation" />
            </xsl:element>
            <xsl:element name="DestinationCity">
               <xsl:value-of select=".//ShipToAddress/@City" />
            </xsl:element>
            <xsl:element name="DestinationState">
               <xsl:value-of select=".//ShipToAddress/@State" />
            </xsl:element>
            <xsl:element name="DestinationPostalCode">
               <xsl:value-of select=".//ShipToAddress/@ZipCode" />
            </xsl:element>
            <xsl:element name="DestinationCountry">
               <xsl:value-of select=".//ShipToAddress/@Country" />
            </xsl:element>
			<xsl:choose>
                        <xsl:when test=".//ShipToAddress/@IsCommercialAddress='Y'">
                            <xsl:element name="DestinationResidentialFlag">
                            <xsl:copy-of select="'N'" />
                            </xsl:element>
                        </xsl:when>
                    <xsl:otherwise>
                            <xsl:element name="DestinationResidentialFlag">
                            <xsl:copy-of select="'Y'" />
                            </xsl:element>                                     
                    </xsl:otherwise>
                 </xsl:choose>
            <xsl:element name="DivisionCode">
               <xsl:value-of select=".//@OrganizationCode" />
            </xsl:element>
            <xsl:element name="EarliestDepartureTime">
               <xsl:value-of select=".//@EarliestShipDate" />
            </xsl:element>
            <xsl:element name="LatestDepartureTime">
               <!--xsl:value-of select="xs:dateTime($vDate) + xs:dayTimeDuration('P10D')" /-->
               <xsl:value-of select=".//@LatestShipDate" />
            </xsl:element>
            <xsl:element name="EarliestArrivalTime">
               <xsl:value-of select=".//@EarliestDeliveryDate" />
            </xsl:element>
            <xsl:element name="LatestArrivalTime">
               <!--xsl:value-of select="xs:dateTime($vDate) + xs:dayTimeDuration('P10D')" /-->
               <xsl:value-of select=".//@LatestDeliveryDate" />
            </xsl:element>
            <xsl:element name="SystemUnitOfMeasureEnumVal">
               <xsl:value-of select="'UMS_IMPERIAL'" />
            </xsl:element>
            <xsl:element name="WeightUnitOfMeasureEnumVal">
               <xsl:value-of select="'UMW_LB'" />
            </xsl:element>
            <!-- Awaiting query confirmation from TMS
                    xsl:element name="LengthUnitOfMeasureEnumVal">
                  <xsl:value-of select="'Value Required'" />
               </xsl:element-->
            <xsl:element name="FreightDetail">
               <xsl:element name="WeightByFreightClass">
                  <xsl:element name="FreightClassCode">
                     <xsl:value-of select="'60'" />
                  </xsl:element>
                  <xsl:element name="FreightClassNominalWeight">
                     <xsl:value-of select=".//@Weight" />
                  </xsl:element>
                  <!-- Awaiting query confirmation from TMS
                   <xsl:element name="Skids">
                     <xsl:value-of select="'Value Required'" />
                  </xsl:element>
               <xsl:element name="CurrencyCode">
                  <xsl:value-of select="./" />
               </xsl:element-->
               </xsl:element>
            </xsl:element>
            <xsl:element name="EnableNewRateCalcFunctionFlag">
               <xsl:value-of select="'true'" />
            </xsl:element>
            <!--xsl:element name="RoutingMethodEnumVal">
                  <xsl:value-of select="'RUTNGMTD_SCHEDULING'" />
               </xsl:element-->
         </xsl:element>
         <!--/xsl:for-each-->
      </xsl:element>
   </xsl:template>
</xsl:stylesheet>


<!-- Sample Input

<PromiseShipments EnterpriseCode="FleetPride" OrganizationCode="FleetPride">
    <PromiseShipment GroupID="1" ShipNode="DA" EarliestShipDate="2021-02-11T12:02:45-05:00" LatestShipDate="2021-02-21T12:02:45-05:00" DeliveryMethod="SHP" DedicatedRouteFlag="N">
		<ShipFromAddress ShipFromLocation="DA" AddressLine1="" AddressLine2="" AddressLine3=""
            City="" Country="" State="" ZipCode=""/>	
        <ShipToAddress ShipToLocation="DUMMY" AddressLine1="" AddressLine2="" AddressLine3=""
            City="Dallas" Country="US" State="TX" ZipCode="75219"/> 
		 <PromiseShipmentLines>
			<PromiseShipmentLine Quantity="" ItemID="Item1" ItemUnitHeight="3" ItemUnitHeightUOM="" ItemUnitLength="1" ItemUnitLengthUOM="" ItemUnitVolume="15.00" ItemUnitVolumeUOM="" ItemUnitWeight="10.00" ItemUnitWeightUOM="" ItemUnitWidth="5" ItemUnitWidthUOM="" ProductClass="" UnitOfMeasure="EACH" ShipMode=""/>
			<PromiseShipmentLine Quantity="" ItemID="Item2" ItemUnitHeight="3" ItemUnitHeightUOM="" ItemUnitLength="1" ItemUnitLengthUOM="" ItemUnitVolume="15.00" ItemUnitVolumeUOM="" ItemUnitWeight="10.00" ItemUnitWeightUOM="" ItemUnitWidth="5" ItemUnitWidthUOM="" ProductClass="" UnitOfMeasure="EACH" ShipMode=""/>		
		  </PromiseShipmentLines>
    </PromiseShipment>
    <PromiseShipment GroupID="2" ShipNode="VS" EarliestShipDate="2021-02-11T12:02:45-05:00" LatestShipDate="2021-02-21T12:02:45-05:00" DeliveryMethod="PICK" DedicatedRouteFlag="Y">
		<ShipFromAddress ShipFromLocation="VS" AddressLine1="" AddressLine2="" AddressLine3=""
            City="" Country="" State="" ZipCode=""/>	
        <ShipToAddress ShipToLocation="DUMMY" AddressLine1="" AddressLine2="" AddressLine3=""
            City="Downey" Country="US" State="CA" ZipCode="90231"/> 
		 <PromiseShipmentLines>
			<PromiseShipmentLine Quantity="" ItemID="Item3" ItemUnitHeight="3" ItemUnitHeightUOM="" ItemUnitLength="1" ItemUnitLengthUOM="" ItemUnitVolume="15.00" ItemUnitVolumeUOM="" ItemUnitWeight="10.00" ItemUnitWeightUOM="" ItemUnitWidth="5" ItemUnitWidthUOM="" ProductClass="" UnitOfMeasure="EACH" ShipMode=""/>
			<PromiseShipmentLine Quantity="" ItemID="Item4" ItemUnitHeight="3" ItemUnitHeightUOM="" ItemUnitLength="1" ItemUnitLengthUOM="" ItemUnitVolume="15.00" ItemUnitVolumeUOM="" ItemUnitWeight="10.00" ItemUnitWeightUOM="" ItemUnitWidth="5" ItemUnitWidthUOM="" ProductClass="" UnitOfMeasure="EACH" ShipMode=""/>		
		  </PromiseShipmentLines>			
    </PromiseShipment>
</PromiseShipments>



Sample Output

<?xml version="1.0" encoding="UTF-8"?>
<CISDocument>
    <ApiHeader>
        <OperationName>processRateQuotation</OperationName>
    </ApiHeader>
    <RateCriteria>
        <ProcessARSideFlag>false</ProcessARSideFlag>
        <ShipFromLocationCode>DA</ShipFromLocationCode>
        <ShipToLocationCode>DUMMY</ShipToLocationCode>
        <DestinationCity>Dallas</DestinationCity>
        <DestinationState>TX</DestinationState>
        <DestinationPostalCode>75219</DestinationPostalCode>
        <DestinationCountry>US</DestinationCountry>
        <DivisionCode>FleetPride</DivisionCode>
        <EarliestArrivalTime>2021-02-11T12:02:45-05:00</EarliestArrivalTime>
        <LatestArrivalTime>2021-02-21T12:02:45-05:00</LatestArrivalTime>
        <SystemUnitOfMeasureEnumVal>UMS_IMPERIAL</SystemUnitOfMeasureEnumVal>
        <WeightUnitOfMeasureEnumVal>UMW_LB</WeightUnitOfMeasureEnumVal>
        <FreightDetail>
            <WeightByFreightClass>
                <FreightClassNominalWeight>10</FreightClassNominalWeight>
            </WeightByFreightClass>
        </FreightDetail>
        <EnableNewRateCalcFunctionFlag>true</EnableNewRateCalcFunctionFlag>
    </RateCriteria>
    <RateCriteria>
        <ProcessARSideFlag>false</ProcessARSideFlag>
        <ShipFromLocationCode>VS</ShipFromLocationCode>
        <ShipToLocationCode>BAK</ShipToLocationCode>
        <DestinationCity>NewYork</DestinationCity>
        <DestinationState/>
        <DestinationPostalCode>75001</DestinationPostalCode>
        <DestinationCountry>US</DestinationCountry>
        <DivisionCode>FleetPride</DivisionCode>
        <EarliestArrivalTime>2021-02-11T12:02:45-05:00</EarliestArrivalTime>
        <LatestArrivalTime>2021-02-21T12:02:45-05:00</LatestArrivalTime>
        <SystemUnitOfMeasureEnumVal>UMS_IMPERIAL</SystemUnitOfMeasureEnumVal>
        <WeightUnitOfMeasureEnumVal>UMW_LB</WeightUnitOfMeasureEnumVal>
        <FreightDetail>
            <WeightByFreightClass>
                <FreightClassNominalWeight>1000</FreightClassNominalWeight>
            </WeightByFreightClass>
        </FreightDetail>
        <EnableNewRateCalcFunctionFlag>true</EnableNewRateCalcFunctionFlag>
    </RateCriteria>
</CISDocument>

-->