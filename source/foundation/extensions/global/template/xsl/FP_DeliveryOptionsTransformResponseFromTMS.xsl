<!-- File Name: FP_DeliveryOptionsTransformResponseFromTMS.xsl

Description: This XSL is to map response received from TMS in to OMS.
  
Modification Log:                
**************************************Initial Draft******************************************** 
Date            Author             Version           Modified Date       Modified by           
2/24/2021       Rucha Chavare       1.0                                                        
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/">
         <xsl:element name="PromiseShipment">
              <xsl:if test=".//ResponseHeader/CompletedSuccessfully= 'true'">
            <xsl:variable name="vSequenceNo">
               <xsl:value-of select=".//Schedule/StopSchedule/StopSequenceNumber" />
            </xsl:variable>
            <xsl:element name="CarrierOptions">
               <xsl:for-each select="CISDocument/ValidLane">
                  <xsl:element name="CarrierOption">
                     <xsl:choose>
                        <xsl:when test="contains(.//ServiceCode,'DEDICATED')"> 
                           <xsl:for-each select=".//Schedule/StopSchedule">
                              <xsl:if test=".//StopSequenceNumber = '2'">
                                 <xsl:attribute name="ExpectedDeliveryDate">
                                    <xsl:value-of select=".//DepartureDateTime" />
                                 </xsl:attribute>
                              </xsl:if>
                           </xsl:for-each>
						    <xsl:attribute name="CarrierServiceCode">
                              <xsl:value-of select="./ServiceCode" />
                           </xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:attribute name="CarrierServiceCode">
                              <xsl:value-of select="./ServiceCode" />
                           </xsl:attribute>
                           <xsl:attribute name="ShippingCharges">
                              <xsl:value-of select="./ChargeAmount" />
                           </xsl:attribute>
                           <xsl:attribute name="Currency">
                              <xsl:value-of select="./CurrencyCode" />
                           </xsl:attribute>
                           <xsl:attribute name="SCAC">
                              <xsl:value-of select="./CarrierCode" />
                           </xsl:attribute>
                           <xsl:for-each select=".//Schedule/StopSchedule">
                              <xsl:if test=".//StopSequenceNumber = '2'">
                                 <xsl:attribute name="ExpectedDeliveryDate">
                                    <xsl:value-of select=".//DepartureDateTime" />
                                 </xsl:attribute>
                              </xsl:if>
                           </xsl:for-each>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:element>
               </xsl:for-each>
            </xsl:element>
                  </xsl:if>
            </xsl:element>
   </xsl:template>
</xsl:stylesheet>



<!-- Sample Input

<?xml version = "1.0" encoding = "UTF-8"?>
<CISDocument>
    <ApiHeader>
        <OperationName>processRateQuotationResponse</OperationName>
    </ApiHeader>
    <ResponseHeader>
        <CompletedSuccessfully>true</CompletedSuccessfully>
    </ResponseHeader>
    <ValidLane>
        <TariffCode>CENF_21_LTL</TariffCode>
        <ServiceCode>LTL</ServiceCode>
        <RateCode>RC_FP</RateCode>
        <EquipmentTypeCode>*DFT</EquipmentTypeCode>
        <ChargeAmount>1519.8</ChargeAmount>
        <ChargeAmountInTariffCurrency>1519.8</ChargeAmountInTariffCurrency>
        <CurrencyCode>USD</CurrencyCode>
        <ExchangeRate>1</ExchangeRate>
        <CarrierCode>CENF</CarrierCode>
        <SchedulingValidFlag>true</SchedulingValidFlag>
        <BestTotalHoursElapsedTime>30.07</BestTotalHoursElapsedTime>
        <ServiceGradeCode>0</ServiceGradeCode>
        <Schedule>
            <StopSchedule>
                <StopSequenceNumber>1</StopSequenceNumber>
                <ArrivalDateTime>2021-01-13T07:30:00</ArrivalDateTime>
                <DepartureDateTime>2021-01-13T07:30:00</DepartureDateTime>
                <WaitingTime>0</WaitingTime>
                <LoadingTIme>0</LoadingTIme>
                <UnloadingTime>0</UnloadingTime>
                <WeekendHolidayBreakHours>0</WeekendHolidayBreakHours>
                <WeekendHolidayBreakOrientationEnumVal>WHBO_NULL</WeekendHolidayBreakOrientationEnumVal>
            </StopSchedule>
            <StopSchedule>
                <StopSequenceNumber>2</StopSequenceNumber>
                <ArrivalDateTime>2021-01-14T14:34:00</ArrivalDateTime>
                <DepartureDateTime>2021-01-14T14:34:00</DepartureDateTime>
                <WaitingTime>0</WaitingTime>
                <LoadingTIme>0</LoadingTIme>
                <UnloadingTime>0</UnloadingTime>
                <WeekendHolidayBreakHours>0</WeekendHolidayBreakHours>
                <WeekendHolidayBreakOrientationEnumVal>WHBO_NULL</WeekendHolidayBreakOrientationEnumVal>
            </StopSchedule>
            <TotalElapsedTime>30.07</TotalElapsedTime>
            <TotalWaitTime>0</TotalWaitTime>
            <WeekendHolidayBreakHours>0</WeekendHolidayBreakHours>
            <DefaultScheduleFlag>false</DefaultScheduleFlag>
        </Schedule>
        <ChargeDetail>
            <ChargeCode>LTL</ChargeCode>
            <ChargeSequence>0</ChargeSequence>
            <ChargeLevelEnumVal>CHL_SERVICE</ChargeLevelEnumVal>
            <ChargeUnits>0</ChargeUnits>
            <LookupUnits>0</LookupUnits>
            <RatingUnits>0</RatingUnits>
            <RateRange>0</RateRange>
            <RatedAs>0</RatedAs>
            <ChargedAmount>0</ChargedAmount>
            <DiscountAmount>0</DiscountAmount>
            <OptionApplyLevelEnumVal>OAL_NULL</OptionApplyLevelEnumVal>
            <ManualOverrideUnits>0</ManualOverrideUnits>
            <ManualOverrideAmount>0</ManualOverrideAmount>
            <ShippingLocationTypeEnumVal>SPT_NULL</ShippingLocationTypeEnumVal>
            <PrePurchasedFlag>false</PrePurchasedFlag>
            <IsTaxFlag>false</IsTaxFlag>
            <OriginalCurrencyCode>USD</OriginalCurrencyCode>
            <OriginalCurrencyExchangeRate>1</OriginalCurrencyExchangeRate>
            <OriginalCurrencyGrossAmount>0</OriginalCurrencyGrossAmount>
            <OriginalCurrencyDiscountAmount>0</OriginalCurrencyDiscountAmount>
            <OriginalCurrencyNetAmount>0</OriginalCurrencyNetAmount>
            <TariffID>1011</TariffID>
        </ChargeDetail>
        <ChargeDetail>
            <ChargeCode>SPML</ChargeCode>
            <ChargeDetailFreightClassCode>*FAK</ChargeDetailFreightClassCode>
            <RatedAsFreightClassCode>*FAK</RatedAsFreightClassCode>
            <ChargeSequence>1</ChargeSequence>
            <ChargeLevelEnumVal>CHL_CONDITION</ChargeLevelEnumVal>
            <ChargeUnits>1266.5</ChargeUnits>
            <LookupUnits>1266.5</LookupUnits>
            <RatingUnits>1266.5</RatingUnits>
            <RateRange>9999999</RateRange>
            <RatedAs>1.2</RatedAs>
            <ChargedAmount>1519.8</ChargedAmount>
            <DiscountAmount>0</DiscountAmount>
            <OptionApplyLevelEnumVal>OAL_NULL</OptionApplyLevelEnumVal>
            <ManualOverrideUnits>1266.5</ManualOverrideUnits>
            <ManualOverrideAmount>1519.8</ManualOverrideAmount>
            <ShippingLocationTypeEnumVal>SPT_NULL</ShippingLocationTypeEnumVal>
            <PrePurchasedFlag>false</PrePurchasedFlag>
            <IsTaxFlag>false</IsTaxFlag>
            <OriginalCurrencyCode>USD</OriginalCurrencyCode>
            <OriginalCurrencyExchangeRate>1</OriginalCurrencyExchangeRate>
            <OriginalCurrencyGrossAmount>1519.8</OriginalCurrencyGrossAmount>
            <OriginalCurrencyDiscountAmount>0</OriginalCurrencyDiscountAmount>
            <OriginalCurrencyNetAmount>1519.8</OriginalCurrencyNetAmount>
            <TariffID>1011</TariffID>
        </ChargeDetail>
        <ChargeDetail>
            <ChargeSequence>2</ChargeSequence>
            <ChargeLevelEnumVal>CHL_TOTAL</ChargeLevelEnumVal>
            <ChargeUnits>0</ChargeUnits>
            <LookupUnits>0</LookupUnits>
            <RatingUnits>0</RatingUnits>
            <RateRange>0</RateRange>
            <RatedAs>0</RatedAs>
            <ChargedAmount>1519.8</ChargedAmount>
            <DiscountAmount>0</DiscountAmount>
            <OptionApplyLevelEnumVal>OAL_NULL</OptionApplyLevelEnumVal>
            <ManualOverrideUnits>0</ManualOverrideUnits>
            <ManualOverrideAmount>1519.8</ManualOverrideAmount>
            <ShippingLocationTypeEnumVal>SPT_NULL</ShippingLocationTypeEnumVal>
            <PrePurchasedFlag>false</PrePurchasedFlag>
            <IsTaxFlag>false</IsTaxFlag>
            <OriginalCurrencyCode>USD</OriginalCurrencyCode>
            <OriginalCurrencyExchangeRate>0</OriginalCurrencyExchangeRate>
            <OriginalCurrencyGrossAmount>1519.8</OriginalCurrencyGrossAmount>
            <OriginalCurrencyDiscountAmount>0</OriginalCurrencyDiscountAmount>
            <OriginalCurrencyNetAmount>1519.8</OriginalCurrencyNetAmount>
            <TariffID>1011</TariffID>
        </ChargeDetail>
        <LegSequenceNumber>0</LegSequenceNumber>
        <ShipFromLocationTypeEnumVal>SPT_NULL</ShipFromLocationTypeEnumVal>
        <ShipToLocationTypeEnumVal>SPT_NULL</ShipToLocationTypeEnumVal>
        <FreightAuctionEligibilityEnumVal>FRHTAUCTELGB_NONE</FreightAuctionEligibilityEnumVal>
        <LaneID>1011</LaneID>
    </ValidLane>
        <ValidLane>
        <TariffCode>CENF_21_LTL</TariffCode>
        <ServiceCode>LTL</ServiceCode>
        <RateCode>RC_FP</RateCode>
        <EquipmentTypeCode>*DFT</EquipmentTypeCode>
        <ChargeAmount>1519.8</ChargeAmount>
        <ChargeAmountInTariffCurrency>1519.8</ChargeAmountInTariffCurrency>
        <CurrencyCode>USD</CurrencyCode>
        <ExchangeRate>1</ExchangeRate>
        <CarrierCode>CENF</CarrierCode>
        <SchedulingValidFlag>true</SchedulingValidFlag>
        <BestTotalHoursElapsedTime>30.07</BestTotalHoursElapsedTime>
        <ServiceGradeCode>0</ServiceGradeCode>
        <Schedule>
            <StopSchedule>
                <StopSequenceNumber>1</StopSequenceNumber>
                <ArrivalDateTime>2021-01-13T07:30:00</ArrivalDateTime>
                <DepartureDateTime>2021-01-13T07:30:00</DepartureDateTime>
                <WaitingTime>0</WaitingTime>
                <LoadingTIme>0</LoadingTIme>
                <UnloadingTime>0</UnloadingTime>
                <WeekendHolidayBreakHours>0</WeekendHolidayBreakHours>
                <WeekendHolidayBreakOrientationEnumVal>WHBO_NULL</WeekendHolidayBreakOrientationEnumVal>
            </StopSchedule>
            <StopSchedule>
                <StopSequenceNumber>2</StopSequenceNumber>
                <ArrivalDateTime>2021-01-14T14:34:00</ArrivalDateTime>
                <DepartureDateTime>2021-01-14T14:34:00</DepartureDateTime>
                <WaitingTime>0</WaitingTime>
                <LoadingTIme>0</LoadingTIme>
                <UnloadingTime>0</UnloadingTime>
                <WeekendHolidayBreakHours>0</WeekendHolidayBreakHours>
                <WeekendHolidayBreakOrientationEnumVal>WHBO_NULL</WeekendHolidayBreakOrientationEnumVal>
            </StopSchedule>
            <TotalElapsedTime>30.07</TotalElapsedTime>
            <TotalWaitTime>0</TotalWaitTime>
            <WeekendHolidayBreakHours>0</WeekendHolidayBreakHours>
            <DefaultScheduleFlag>false</DefaultScheduleFlag>
        </Schedule>
        <ChargeDetail>
            <ChargeCode>LTL</ChargeCode>
            <ChargeSequence>0</ChargeSequence>
            <ChargeLevelEnumVal>CHL_SERVICE</ChargeLevelEnumVal>
            <ChargeUnits>0</ChargeUnits>
            <LookupUnits>0</LookupUnits>
            <RatingUnits>0</RatingUnits>
            <RateRange>0</RateRange>
            <RatedAs>0</RatedAs>
            <ChargedAmount>0</ChargedAmount>
            <DiscountAmount>0</DiscountAmount>
            <OptionApplyLevelEnumVal>OAL_NULL</OptionApplyLevelEnumVal>
            <ManualOverrideUnits>0</ManualOverrideUnits>
            <ManualOverrideAmount>0</ManualOverrideAmount>
            <ShippingLocationTypeEnumVal>SPT_NULL</ShippingLocationTypeEnumVal>
            <PrePurchasedFlag>false</PrePurchasedFlag>
            <IsTaxFlag>false</IsTaxFlag>
            <OriginalCurrencyCode>USD</OriginalCurrencyCode>
            <OriginalCurrencyExchangeRate>1</OriginalCurrencyExchangeRate>
            <OriginalCurrencyGrossAmount>0</OriginalCurrencyGrossAmount>
            <OriginalCurrencyDiscountAmount>0</OriginalCurrencyDiscountAmount>
            <OriginalCurrencyNetAmount>0</OriginalCurrencyNetAmount>
            <TariffID>1011</TariffID>
        </ChargeDetail>
        <ChargeDetail>
            <ChargeCode>SPML</ChargeCode>
            <ChargeDetailFreightClassCode>*FAK</ChargeDetailFreightClassCode>
            <RatedAsFreightClassCode>*FAK</RatedAsFreightClassCode>
            <ChargeSequence>1</ChargeSequence>
            <ChargeLevelEnumVal>CHL_CONDITION</ChargeLevelEnumVal>
            <ChargeUnits>1266.5</ChargeUnits>
            <LookupUnits>1266.5</LookupUnits>
            <RatingUnits>1266.5</RatingUnits>
            <RateRange>9999999</RateRange>
            <RatedAs>1.2</RatedAs>
            <ChargedAmount>1519.8</ChargedAmount>
            <DiscountAmount>0</DiscountAmount>
            <OptionApplyLevelEnumVal>OAL_NULL</OptionApplyLevelEnumVal>
            <ManualOverrideUnits>1266.5</ManualOverrideUnits>
            <ManualOverrideAmount>1519.8</ManualOverrideAmount>
            <ShippingLocationTypeEnumVal>SPT_NULL</ShippingLocationTypeEnumVal>
            <PrePurchasedFlag>false</PrePurchasedFlag>
            <IsTaxFlag>false</IsTaxFlag>
            <OriginalCurrencyCode>USD</OriginalCurrencyCode>
            <OriginalCurrencyExchangeRate>1</OriginalCurrencyExchangeRate>
            <OriginalCurrencyGrossAmount>1519.8</OriginalCurrencyGrossAmount>
            <OriginalCurrencyDiscountAmount>0</OriginalCurrencyDiscountAmount>
            <OriginalCurrencyNetAmount>1519.8</OriginalCurrencyNetAmount>
            <TariffID>1011</TariffID>
        </ChargeDetail>
        <ChargeDetail>
            <ChargeSequence>2</ChargeSequence>
            <ChargeLevelEnumVal>CHL_TOTAL</ChargeLevelEnumVal>
            <ChargeUnits>0</ChargeUnits>
            <LookupUnits>0</LookupUnits>
            <RatingUnits>0</RatingUnits>
            <RateRange>0</RateRange>
            <RatedAs>0</RatedAs>
            <ChargedAmount>1519.8</ChargedAmount>
            <DiscountAmount>0</DiscountAmount>
            <OptionApplyLevelEnumVal>OAL_NULL</OptionApplyLevelEnumVal>
            <ManualOverrideUnits>0</ManualOverrideUnits>
            <ManualOverrideAmount>1519.8</ManualOverrideAmount>
            <ShippingLocationTypeEnumVal>SPT_NULL</ShippingLocationTypeEnumVal>
            <PrePurchasedFlag>false</PrePurchasedFlag>
            <IsTaxFlag>false</IsTaxFlag>
            <OriginalCurrencyCode>USD</OriginalCurrencyCode>
            <OriginalCurrencyExchangeRate>0</OriginalCurrencyExchangeRate>
            <OriginalCurrencyGrossAmount>1519.8</OriginalCurrencyGrossAmount>
            <OriginalCurrencyDiscountAmount>0</OriginalCurrencyDiscountAmount>
            <OriginalCurrencyNetAmount>1519.8</OriginalCurrencyNetAmount>
            <TariffID>1011</TariffID>
        </ChargeDetail>
        <LegSequenceNumber>0</LegSequenceNumber>
        <ShipFromLocationTypeEnumVal>SPT_NULL</ShipFromLocationTypeEnumVal>
        <ShipToLocationTypeEnumVal>SPT_NULL</ShipToLocationTypeEnumVal>
        <FreightAuctionEligibilityEnumVal>FRHTAUCTELGB_NONE</FreightAuctionEligibilityEnumVal>
        <LaneID>1011</LaneID>
    </ValidLane>
</CISDocument>


Sample Output

<?xml version="1.0" encoding="UTF-8"?>
<PromiseShipments>
    <PromiseShipment>
        <CarrierOptions>
            <CarrierOption ShippingCharges="1519.8" CarrierServiceCode="LTL" Currency="USD" SCAC="CENF" ExpectedDeliveryDate="2021-01-13T07:30:00"/>
        </CarrierOptions>
    </PromiseShipment>
    <PromiseShipment>
        <CarrierOptions>
            <CarrierOption ShippingCharges="1519.8" CarrierServiceCode="LTL" Currency="USD" SCAC="CENF" ExpectedDeliveryDate="2021-01-13T07:30:00"/>
        </CarrierOptions>
    </PromiseShipment>
</PromiseShipments>



-->