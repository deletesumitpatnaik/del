<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
    <Promise>
    <xsl:attribute name="AllocationRuleID">
         <xsl:value-of select="Order/@AllocationRuleID"/>
        </xsl:attribute>
    <xsl:attribute name="CheckInventory">
         <xsl:value-of select="'N'"/>
        </xsl:attribute>
    <xsl:attribute name="AllocationRuleID">
         <xsl:value-of select="Order/@AllocationRuleID"/>
        </xsl:attribute>
   <xsl:attribute name="EnterpriseCode">
         <xsl:value-of select="Order/@EnterpriseCode"/>
        </xsl:attribute>
	<xsl:attribute name="ShipNode">
         <xsl:value-of select="Order/OrderLines/OrderLine/@ShipNode"/>
        </xsl:attribute>
	<xsl:attribute name="DistanceToConsider">
        <xsl:value-of select="'500'"/>
        </xsl:attribute>
    <xsl:attribute name="DistanceUOMToConsider">
         <xsl:value-of select="'MILE'"/>
        </xsl:attribute>
	<xsl:attribute name="FulfillmentType">
        <xsl:value-of select="'VS'"/>
        </xsl:attribute>
	<xsl:attribute name="DistributionRuleId">
         <xsl:value-of select="Order/OrderLines/OrderLine/@DistributionRuleId"/>
        </xsl:attribute>
	<xsl:attribute name="OrganizationCode">
         <xsl:value-of select="Order/@SellerOrganizationCode"/>
        </xsl:attribute>
	<xsl:attribute name="ShipNode">
         <xsl:value-of select="Order/OrderLines/OrderLine/@ShipNode"/>
        </xsl:attribute>
	<ShipToAddress>
   <xsl:attribute name="AddressLine1">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine1"/>
        </xsl:attribute>
    <xsl:attribute name="AddressLine2">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine2"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine3">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine3"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine4">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine4"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine5">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine5"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine6">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine6"/>
    </xsl:attribute>       
    <xsl:attribute name="AlternateEmailID">
        <xsl:value-of select="Order/PersonInfoShipTo/@AlternateEmailID"/>
    </xsl:attribute>
    <xsl:attribute name="Beeper">
         <xsl:value-of select="Order/PersonInfoShipTo/@Beeper"/>
    </xsl:attribute>
    <xsl:attribute name="City">
         <xsl:value-of select="Order/PersonInfoShipTo/@City"/>
    </xsl:attribute>   
    <xsl:attribute name="Company">
        <xsl:value-of select="Order/PersonInfoShipTo/@Company"/>
    </xsl:attribute>   
    <xsl:attribute name="Country">
        <xsl:value-of select="Order/PersonInfoShipTo/@Country"/>
    </xsl:attribute>   
    <xsl:attribute name="DayFaxNo">
        <xsl:value-of select="Order/PersonInfoShipTo/@DayFaxNo"/>
    </xsl:attribute>   
    <xsl:attribute name="DayPhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@DayPhone"/>
    </xsl:attribute>   
    <xsl:attribute name="Department">
        <xsl:value-of select="Order/PersonInfoShipTo/@Department"/>
    </xsl:attribute>   
    <xsl:attribute name="EMailID">
        <xsl:value-of select="Order/PersonInfoShipTo/@EMailID"/>
    </xsl:attribute>   
    <xsl:attribute name="EveningFaxNo">
        <xsl:value-of select="Order/PersonInfoShipTo/@EveningFaxNo"/>
    </xsl:attribute>   
    <xsl:attribute name="EveningPhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@EveningPhone"/>
    </xsl:attribute>
    <xsl:attribute name="FirstName">
        <xsl:value-of select="Order/PersonInfoShipTo/@FirstName"/>
    </xsl:attribute>
    <xsl:attribute name="IsCommercialAddress">
        <xsl:value-of select="Order/PersonInfoShipTo/@IsCommercialAddress"/>
    </xsl:attribute>
    <xsl:attribute name="JobTitle">
        <xsl:value-of select="Order/@JobTitle"/>
    </xsl:attribute>
    <xsl:attribute name="LastName">
        <xsl:value-of select="Order/PersonInfoShipTo/@LastName"/>
    </xsl:attribute>
    <xsl:attribute name="MiddleName">
        <xsl:value-of select="Order/PersonInfoShipTo/@MiddleName"/>
    </xsl:attribute>   
    <xsl:attribute name="MobilePhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@MobilePhone"/>
    </xsl:attribute>   
    <xsl:attribute name="OtherPhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@OtherPhone"/>
    </xsl:attribute>   
    <xsl:attribute name="PersonID">
        <xsl:value-of select="Order/PersonInfoShipTo/@PersonID"/>
    </xsl:attribute>   
    <xsl:attribute name="PreferredShipAddress">
        <xsl:value-of select="Order/PersonInfoShipTo/@PreferredShipAddress"/>
    </xsl:attribute>   
    <xsl:attribute name="State">
        <xsl:value-of select="Order/PersonInfoShipTo/@State"/>
    </xsl:attribute>   
    <xsl:attribute name="Suffix">
        <xsl:value-of select="Order/PersonInfoShipTo/@Suffix"/>
    </xsl:attribute>   
    <xsl:attribute name="TaxGeoCode">
        <xsl:value-of select="Order/PersonInfoShipTo/@TaxGeoCode"/>
    </xsl:attribute>   
    <xsl:attribute name="Title">
        <xsl:value-of select="Order/PersonInfoShipTo/@Title"/>
    </xsl:attribute>   
    <xsl:attribute name="ZipCode">
        <xsl:value-of select="Order/PersonInfoShipTo/@ZipCode"/>
    </xsl:attribute>       
    </ShipToAddress>
   <PromiseLines>
   <PromiseLine>
    <xsl:attribute name="DepartmentCode">
        <xsl:value-of select="Order/PersonInfoShipTo/@DepartmentCode"/>
        </xsl:attribute>
	<xsl:attribute name="DistributionRuleId">
         <xsl:value-of select="Order/OrderLines/OrderLine/@DistributionRuleId"/>
        </xsl:attribute>
	<xsl:attribute name="ItemID">
         <xsl:value-of select="Order/OrderLines/OrderLine/Item/@ItemID"/>
        </xsl:attribute>
	<xsl:attribute name="RequiredQty">
         <xsl:value-of select="'1'"/>
        </xsl:attribute>
	<xsl:attribute name="FreightTerms">
        <xsl:value-of select="Order/PersonInfoShipTo/@FreightTerms"/>
        </xsl:attribute>
	<xsl:attribute name="FulfillmentType">
        <xsl:value-of select="'VS'"/>
        </xsl:attribute>
	<xsl:attribute name="LevelOfService">
        <xsl:value-of select="Order/PersonInfoShipTo/@LevelOfService"/>
        </xsl:attribute>
	<xsl:attribute name="LineId">
        <xsl:value-of select="Order/OrderLines/OrderLine/Extn/@ExtnLineID"/>
        </xsl:attribute>
	<xsl:attribute name="ShipNode">
        <xsl:value-of select="Order/OrderLines/OrderLine/@ShipNode"/>
        </xsl:attribute>
	<xsl:attribute name="UnitOfMeasure">
        <xsl:value-of select="Order/Item/@UnitOfMeasure"/>
        </xsl:attribute>
	<ShipToAddress>
	<xsl:attribute name="AddressLine1">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine1"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine2">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine2"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine3">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine3"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine4">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine4"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine5">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine5"/>
    </xsl:attribute>
    <xsl:attribute name="AddressLine6">
        <xsl:value-of select="Order/PersonInfoShipTo/@AddressLine6"/>
    </xsl:attribute>       
    <xsl:attribute name="AlternateEmailID">
        <xsl:value-of select="Order/PersonInfoShipTo/@AlternateEmailID"/>
    </xsl:attribute>
    <xsl:attribute name="Beeper">
         <xsl:value-of select="Order/PersonInfoShipTo/@Beeper"/>
    </xsl:attribute>
    <xsl:attribute name="City">
         <xsl:value-of select="Order/PersonInfoShipTo/@City"/>
    </xsl:attribute>   
    <xsl:attribute name="Company">
        <xsl:value-of select="Order/PersonInfoShipTo/@Company"/>
    </xsl:attribute>   
    <xsl:attribute name="Country">
        <xsl:value-of select="Order/PersonInfoShipTo/@Country"/>
    </xsl:attribute>   
    <xsl:attribute name="DayFaxNo">
        <xsl:value-of select="Order/PersonInfoShipTo/@DayFaxNo"/>
    </xsl:attribute>   
    <xsl:attribute name="DayPhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@DayPhone"/>
    </xsl:attribute>   
    <xsl:attribute name="Department">
        <xsl:value-of select="Order/PersonInfoShipTo/@Department"/>
    </xsl:attribute>   
    <xsl:attribute name="EMailID">
        <xsl:value-of select="Order/PersonInfoShipTo/@EMailID"/>
    </xsl:attribute>   
    <xsl:attribute name="EveningFaxNo">
        <xsl:value-of select="Order/PersonInfoShipTo/@EveningFaxNo"/>
    </xsl:attribute>   
    <xsl:attribute name="EveningPhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@EveningPhone"/>
    </xsl:attribute>
    <xsl:attribute name="FirstName">
        <xsl:value-of select="Order/PersonInfoShipTo/@FirstName"/>
    </xsl:attribute>
    <xsl:attribute name="IsCommercialAddress">
        <xsl:value-of select="Order/PersonInfoShipTo/@IsCommercialAddress"/>
    </xsl:attribute>
    <xsl:attribute name="JobTitle">
        <xsl:value-of select="Order/@JobTitle"/>
    </xsl:attribute>
    <xsl:attribute name="LastName">
        <xsl:value-of select="Order/PersonInfoShipTo/@LastName"/>
    </xsl:attribute>
    <xsl:attribute name="MiddleName">
        <xsl:value-of select="Order/PersonInfoShipTo/@MiddleName"/>
    </xsl:attribute>   
    <xsl:attribute name="MobilePhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@MobilePhone"/>
    </xsl:attribute>   
    <xsl:attribute name="OtherPhone">
        <xsl:value-of select="Order/PersonInfoShipTo/@OtherPhone"/>
    </xsl:attribute>   
    <xsl:attribute name="PersonID">
        <xsl:value-of select="Order/PersonInfoShipTo/@PersonID"/>
    </xsl:attribute>   
    <xsl:attribute name="PreferredShipAddress">
        <xsl:value-of select="Order/PersonInfoShipTo/@PreferredShipAddress"/>
    </xsl:attribute>   
    <xsl:attribute name="State">
        <xsl:value-of select="Order/PersonInfoShipTo/@State"/>
    </xsl:attribute>   
    <xsl:attribute name="Suffix">
        <xsl:value-of select="Order/PersonInfoShipTo/@Suffix"/>
    </xsl:attribute>   
    <xsl:attribute name="TaxGeoCode">
        <xsl:value-of select="Order/PersonInfoShipTo/@TaxGeoCode"/>
    </xsl:attribute>   
    <xsl:attribute name="Title">
        <xsl:value-of select="Order/PersonInfoShipTo/@Title"/>
    </xsl:attribute>   
    <xsl:attribute name="ZipCode">
        <xsl:value-of select="Order/PersonInfoShipTo/@ZipCode"/>
    </xsl:attribute>       
    </ShipToAddress>
	</PromiseLine>
</PromiseLines>
   </Promise>
   </xsl:template>
</xsl:stylesheet>
