package com.fp.oms.util;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;

public class CommonOrderUtils {
	private static YFCLogCategory log = YFCLogCategory.instance(CommonOrderUtils.class);
	/**
	 * 
	 * This method will invoke the API based on the Arguments passed and returns
	 * the output in Document format	 * 
	 * @param env
	 * @param serviceName
	 * @param inputDoc
	 * @param template
	 * @return
	 * @throws Exception
	 */
	public static Document invokeApiWithTemplate(YFSEnvironment env, String serviceName, Document inputDoc,String template) throws Exception {

		YIFApi api = YIFClientFactory.getInstance().getApi();
		env.setApiTemplate(serviceName,template);
		return api.invoke(env,serviceName,inputDoc);
	}

	/**
	 * 
	 * This method will invoke the service based on the Arguments passed and
	 * returns the output in Document format.
	 * 
	 * @param env
	 * @param serviceName
	 * @param inputDoc
	 * @return
	 * @throws Exception
	 */
	public static Document invokeService(YFSEnvironment env, String serviceName, Document inputDoc) throws Exception {

		YIFApi oApi = YIFClientFactory.getInstance().getLocalApi();
		return oApi.executeFlow(env, serviceName, inputDoc);
	}
	/**
	 * 
	 * This method will invoke the service based on the Arguments passed and
	 * returns the output in Document format.
	 * 
	 * @param env
	 * @param serviceName
	 * @param inputDoc
	 * @return
	 * @throws Exception
	 */
	public static Document invokeAPI(YFSEnvironment env, String serviceName, Document inputDoc) throws Exception {

		YIFApi api = YIFClientFactory.getInstance().getApi();
		return api.invoke(env,serviceName,inputDoc);
	}
	
	public static void copyAttributes(Element fromElement, Element toElement) {
		NamedNodeMap attributes = fromElement.getAttributes();
		for (int i = 0; i < attributes.getLength(); i++) {
			Attr node = (Attr) attributes.item(i);
			toElement.setAttributeNS(node.getNamespaceURI(), node.getName(), node.getValue());
		}
	}
   
	


}
