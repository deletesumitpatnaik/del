package com.fp.oms.api.inventory.fulfillmentOptions;

import java.io.FileInputStream;
import java.io.InputStream;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.fp.oms.util.CommonConstants;
import com.infosys.es.scm.coe.util.xml.XMLUtil;
import com.yantra.ycp.ui.screens.util.YCPCommonCodeScreenUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * Class to create a response for sales force.
 *@version 1.0
 */
public class FulfillmentOptionsWrapper {
	
	private static YFCLogCategory log = YFCLogCategory.instance(FulfillmentOptionsWrapper.class);
	/**
	 * Method to prepare response for SalesForce for delivery and fulfillment options.
	 * This class merge the output of findinventory api with option as priority and with option as cost.
	 * @Version 1.0
	 */
	public Document prepareResponseForSalesForce(YFSEnvironment env,Document docInput){	
//		public Document prepareResponseForSalesForce(){	
		log.verbose("Inside prepareResponseForSalesForce");
		log.beginTimer("prepareResponseForSalesForce");


		//Document doc = null;
		Document docPromise = null;
	//	InputStream is = null;
		//YIFApi oApi = YIFClientFactory.getInstance().getLocalApi();   
		try{
		//is = new FileInputStream("findInventory_multiapi_output.xml");
	//	Document docInput = XMLUtil.getDocument(is);
		
		//System.out.println("documentElement:" + doc.getDocumentElement());
		//	log.verbose("Inside prepareResponseForSalesForce" + XMLUtil.getXMLString(docInput) );
		YFCElement yfcinxmlElem = YFCDocument.getDocumentFor(docInput).getDocumentElement();
		docPromise =  XMLUtil.createDocument(CommonConstants.PROMISE);
		Element elePromise = XMLUtil.getElementByXPath(docInput,CommonConstants.XPATH_PROMISE);
		Element elePromiseOutput = docPromise.getDocumentElement();
		elePromiseOutput.setAttribute(CommonConstants.ENTERPRISE_CODE,elePromise.getAttribute(CommonConstants.ENTERPRISE_CODE));
		elePromiseOutput.setAttribute(CommonConstants.ORGANIZATION_CODE, elePromise.getAttribute(CommonConstants.ORGANIZATION_CODE));
		Element eleShipTOAddressOutput =  XMLUtil.createElement(docPromise,CommonConstants.SHIP_TO_ADDRESS, null);
		Element eleShipToAddressInput = XMLUtil.getElementByXPath(docInput, CommonConstants.XPATH_SHIP_TO_ADDRESS);
		if(eleShipToAddressInput != null) {
		XMLUtil.copyElement(docPromise, eleShipToAddressInput, eleShipTOAddressOutput);
		XMLUtil.appendChild(elePromiseOutput, eleShipTOAddressOutput);
		}
		log.verbose("Inside prepareResponseForSalesForce");
		Element eleOptions = XMLUtil.createElement(docPromise,CommonConstants.OPTIONS, null);
		
		YFCNodeList<YFCElement> nlApi = yfcinxmlElem.getElementsByTagName(CommonConstants.API);
		//Looping through COST And Priority options
	    for(YFCElement eleApi : nlApi) {	
	    	log.verbose("Inside API for-loop");
	    	YFCElement yfceleOutput = eleApi.getChildElement(CommonConstants.OUTPUT);
	    	YFCElement yfcelePromise = yfceleOutput.getChildElement(CommonConstants.PROMISE);
	    	YFCElement yfceleChildOutput = yfcelePromise.getChildElement(CommonConstants.OUTPUT);
	    	YFCElement yfceleChildPromise = yfceleChildOutput.getChildElement(CommonConstants.PROMISE);
	    	YFCElement yfceleOptions = yfceleChildPromise.getChildElement(CommonConstants.OPTIONS);
	    	if(yfceleOptions != null) {
	    	YFCElement yfceleOption = yfceleOptions.getChildElement(CommonConstants.OPTION);	
	    	
	    	 Element eleOption = XMLUtil.createElement(docPromise,CommonConstants.OPTION, null);
	    	 
	    	 eleOption.setAttribute(CommonConstants.ALLOCATION_RULE_ID, yfceleOption.getAttribute(CommonConstants.ALLOCATION_RULE_ID));
	    	// eleOption.setAttribute(CommonConstants.FIRST_DATE, yfceleOption.getAttribute("FirstDate"));
	    	 eleOption.setAttribute(CommonConstants.HAS_ANY_UNAVAILABLE_QTY, yfceleOption.getAttribute(CommonConstants.HAS_ANY_UNAVAILABLE_QTY));
	    	 eleOption.setAttribute(CommonConstants.LAST_DATE, yfceleOption.getAttribute(CommonConstants.LAST_DATE));
	    	 eleOption.setAttribute(CommonConstants.TOTAL_SHIPMENTS, yfceleOption.getAttribute(CommonConstants.TOTAL_SHIPMENTS));
			    XMLUtil.appendChild(eleOptions, eleOption);
			    
	    	 YFCElement yfcelePromiseShipments = yfceleOption.getChildElement(CommonConstants.PROMISE_SHIPMENTS);
	    	 YFCNodeList<YFCElement> nlPromiseShipment = yfcelePromiseShipments.getElementsByTagName(CommonConstants.PROMISE_SHIPMENT);	    	 
	    	 Element elePromiseShipments = XMLUtil.createElement(docPromise,CommonConstants.PROMISE_SHIPMENTS, null);
	    	 elePromiseShipments.setAttribute(CommonConstants.ALLOCATION_RULE_ID, yfcelePromiseShipments.getAttribute(CommonConstants.ALLOCATION_RULE_ID));
	    	 elePromiseShipments.setAttribute(CommonConstants.ENTERPRISE_CODE, yfcelePromiseShipments.getAttribute(CommonConstants.ENTERPRISE_CODE));	    	 
	    	 XMLUtil.appendChild(eleOption, elePromiseShipments);
	    	 String sShipMode = null;
	    	 for(YFCElement yfcelePromiseShipment : nlPromiseShipment) {
	    		 log.verbose("--------------Inside promise shipment for loop -----------");
	    		 Element elePromiseShipment = XMLUtil.createElement(docPromise,CommonConstants.PROMISE_SHIPMENT, null);
	    		 elePromiseShipment.setAttribute(CommonConstants.DELIVERY_METHOD, yfcelePromiseShipment.getAttribute(CommonConstants.DELIVERY_METHOD));
	    		 elePromiseShipment.setAttribute(CommonConstants.GROUP_ID, yfcelePromiseShipment.getAttribute(CommonConstants.GROUP_ID));
	    		 elePromiseShipment.setAttribute(CommonConstants.SHIP_DATE, yfcelePromiseShipment.getAttribute(CommonConstants.SHIP_DATE));
	    		 elePromiseShipment.setAttribute(CommonConstants.SHIP_NODE, yfcelePromiseShipment.getAttribute(CommonConstants.SHIP_NODE));
	    		 if((elePromise.getAttribute(CommonConstants.ORGANIZATION_CODE).equals(CommonConstants.SELLER_ORGANIZATION_PDC)) 
	    				 ||((elePromise.getAttribute(CommonConstants.ORGANIZATION_CODE).equals(CommonConstants.SELLER_ORGANIZATION_FLEETPRIDE_DIRECT)) )){
	    			 if((CommonConstants.DELIVERY_METHOD_SHP).equals(yfcelePromiseShipment.getAttribute(CommonConstants.DELIVERY_METHOD))){
	    		 elePromiseShipment.setAttribute(CommonConstants.DISPLAY_FREIGHT_CHARGES, yfcelePromiseShipment.getAttribute(CommonConstants.DISPLAY_FREIGHT_CHARGES));
	    			 }
	    		 }
	    		 if( null != (yfcelePromiseShipment.getAttribute(CommonConstants.CARRIER_OPTIONS_NOT_AVAILABLE))) {
		    		 elePromiseShipment.setAttribute(CommonConstants.CARRIER_OPTIONS_NOT_AVAILABLE, yfcelePromiseShipment.getAttribute(CommonConstants.CARRIER_OPTIONS_NOT_AVAILABLE));
		    		 }
	    		 Element elePromiseShipmentLines = XMLUtil.createElement(docPromise,CommonConstants.PROMISE_SHIPMENT_LINES, null);
	    		 YFCElement yfcelePromiseShipmentLines = yfcelePromiseShipment.getChildElement(CommonConstants.PROMISE_SHIPMENT_LINES);
		    	 YFCNodeList<YFCElement> nlPromiseShipmentLine = yfcelePromiseShipmentLines.getElementsByTagName(CommonConstants.PROMISE_SHIPMENT_LINE);
		    	 for(YFCElement yfcelePromiseShipmentLine : nlPromiseShipmentLine) {
		    		 log.verbose("Inside promise shipment line for-loop  ");
		    		 Element elePromiseShipmentLine = XMLUtil.createElement(docPromise,CommonConstants.PROMISE_SHIPMENT_LINE, null);
		    		 
		    	//	if((CommonConstants.DELIVERY_METHOD_SHP).equals(yfcelePromiseShipmentLine.getAttribute(CommonConstants.DELIVERY_METHOD))){
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_HEIGHT, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_HEIGHT));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_HEIGHT_UOM, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_HEIGHT_UOM));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_LENGTH, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_LENGTH));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_LENGTH_UOM, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_LENGTH_UOM));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_VOLUME, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_VOLUME));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_VOLUME_UOM, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_VOLUME_UOM));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_WEIGHT, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_WEIGHT));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_WEIGHT_UOM, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_WEIGHT_UOM));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_WIDTH,yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_WIDTH));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_UNIT_WIDTH_UOM, yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_UNIT_WIDTH_UOM));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.SHIPMENT_MODE, yfcelePromiseShipmentLine.getAttribute(CommonConstants.SHIPMENT_MODE));
		    		   
		    		  // sShipMode = yfcelePromiseShipmentLine.getAttribute(CommonConstants.SHIPMENT_MODE);
		    		  // elePromiseShipment.setAttribute(CommonConstants.SHIPMENT_MODE, sShipMode);
		    	//	 }
		    		 elePromiseShipmentLine.setAttribute(CommonConstants.PRODUCT_CLASS, yfcelePromiseShipmentLine.getAttribute(CommonConstants.PRODUCT_CLASS));
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.UNIT_OF_MEASURE, yfcelePromiseShipmentLine.getAttribute(CommonConstants.UNIT_OF_MEASURE));
		    		   //elePromiseShipmentLine.setAttribute(CommonConstants.SHIPPING_CHARGE,yfcelePromiseShipmentLine.getAttribute("ShippingCharges"));	
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.AVAILABLE_QUANTITY,yfcelePromiseShipmentLine.getAttribute(CommonConstants.AVAILABLE_QUANTITY));	
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.ITEM_ID,yfcelePromiseShipmentLine.getAttribute(CommonConstants.ITEM_ID));
		    		  
		    		   elePromiseShipmentLine.setAttribute(CommonConstants.FULFILLMENT_TYPE,yfcelePromiseShipmentLine.getAttribute(CommonConstants.FULFILLMENT_TYPE));
		    		   
		    		   //creating Assignments
					   YFCElement yfceleAssignments = yfcelePromiseShipmentLine.getChildElement(CommonConstants.ASSIGNMENTS);
					   YFCIterable<YFCElement> yfcNLAssignment = yfceleAssignments.getChildren();
					   Element eleAssignments = XMLUtil.createElement(docPromise,CommonConstants.ASSIGNMENTS, null);
					   for(YFCElement yfceleAssignment : yfcNLAssignment) {
						   log.verbose("-Inside promise shipment line assignment for-loop ");
					   Element eleAssignment = XMLUtil.createElement(docPromise,CommonConstants.ASSIGNMENT, null);
					   eleAssignment.setAttribute(CommonConstants.DELIVERY_DATE, yfceleAssignment.getAttribute(CommonConstants.DELIVERY_DATE) );
					   eleAssignment.setAttribute(CommonConstants.INTERACTION_NO, yfceleAssignment.getAttribute(CommonConstants.INTERACTION_NO));
					   eleAssignment.setAttribute(CommonConstants.PROCURED_QTY, yfceleAssignment.getAttribute(CommonConstants.PROCURED_QTY));
					   eleAssignment.setAttribute(CommonConstants.QUANTITY, yfceleAssignment.getAttribute(CommonConstants.QUANTITY));
					   eleAssignment.setAttribute(CommonConstants.SHIP_DATE, yfceleAssignment.getAttribute(CommonConstants.SHIP_DATE));
					   eleAssignment.setAttribute(CommonConstants.EMPTY_ASSIGNMENT_REASON, yfceleAssignment.getAttribute(CommonConstants.EMPTY_ASSIGNMENT_REASON));
					   eleAssignment.setAttribute(CommonConstants.SHIP_NODE, yfceleAssignment.getAttribute(CommonConstants.SHIP_NODE));
					   log.verbose("Inside promise shipment line assignment for-loop ");
					   YFCElement yfcProcurements = yfceleAssignment.getChildElement(CommonConstants.PROCUREMENTS);

					    if(null != yfcProcurements) {
					   YFCIterable<YFCElement> yfcnProcurement = yfcProcurements.getChildren();
					   Element eleProcurements = XMLUtil.createElement(docPromise,CommonConstants.PROCUREMENTS, null);
					   if(null != yfcnProcurement) {
						   
						for(YFCElement yfcProcurement:yfcnProcurement ) {
							 log.verbose("-Inside promise shipment line procurement for loop ");
					   Element eleProcurement = XMLUtil.createElement(docPromise,CommonConstants.PROCUREMENT, null);
					   
					   eleProcurement.setAttribute(CommonConstants.PROCURE_FROM_NODE, yfcProcurement.getAttribute(CommonConstants.PROCURE_FROM_NODE));
					   eleProcurement.setAttribute(CommonConstants.PRODUCT_AVAIL_DATE, yfcProcurement.getAttribute(CommonConstants.PRODUCT_AVAIL_DATE));
					   eleProcurement.setAttribute(CommonConstants.QUANTITY, yfcProcurement.getAttribute(CommonConstants.QUANTITY));
					   eleProcurement.setAttribute(CommonConstants.SHIP_DATE, yfcProcurement.getAttribute(CommonConstants.SHIP_DATE));
					   
					   XMLUtil.appendChild(eleProcurements, eleProcurement);
					   }
					   XMLUtil.appendChild(eleAssignment, eleProcurements);
					   }
					    }
					   XMLUtil.appendChild(eleAssignments, eleAssignment);
						 log.verbose("-Inside promise shipment appending assignment");
					   XMLUtil.appendChild(elePromiseShipmentLine, eleAssignments);
					   log.verbose("-Inside promise shipment lineappending assignments ");
					   }//yfcNLAssignment
					   XMLUtil.appendChild(elePromiseShipmentLines, elePromiseShipmentLine);
					   }//yfcNLPromiseShipmentLines
		    	   YFCElement yfcelePersonInfoShipTo = yfcelePromiseShipment.getChildElement(CommonConstants.SHIP_TO_ADDRESS);
		    		Element eleShipToAddress = XMLUtil.createElement(docPromise,CommonConstants.SHIP_TO_ADDRESS, null);
		    		eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_1, yfcelePersonInfoShipTo.getAttribute(CommonConstants.ADDRESS_LINE_1) );
		    		eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_2, yfcelePersonInfoShipTo.getAttribute(CommonConstants.ADDRESS_LINE_2) );
		    		eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_3, yfcelePersonInfoShipTo.getAttribute(CommonConstants.ADDRESS_LINE_3));
		    		eleShipToAddress.setAttribute(CommonConstants.CITY, yfcelePersonInfoShipTo.getAttribute(CommonConstants.CITY));
		    		eleShipToAddress.setAttribute(CommonConstants.COUNTRY, yfcelePersonInfoShipTo.getAttribute(CommonConstants.COUNTRY));
		    		eleShipToAddress.setAttribute(CommonConstants.STATE, yfcelePersonInfoShipTo.getAttribute(CommonConstants.STATE));
		    		eleShipToAddress.setAttribute(CommonConstants.ZIP_CODE, yfcelePersonInfoShipTo.getAttribute(CommonConstants.ZIP_CODE));
		    		eleShipToAddress.setAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS, yfcelePersonInfoShipTo.getAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS));
		    		XMLUtil.appendChild(elePromiseShipment, eleShipToAddress);
					 		
					// String sValidationResult =  valiatePromiseShipment(elePromiseShipment,elePromiseOutput.getAttribute("EnterpriseCode"));
					
					   YFCElement yfceleCarrierOptions = yfcelePromiseShipment.getChildElement(CommonConstants.CARRIER_OPTIONS);	
					   if(yfceleCarrierOptions!=null) {
						   Element eleCarrierOptions = XMLUtil.createElement(docPromise,CommonConstants.CARRIER_OPTIONS, null);
						   YFCNodeList<YFCElement> yfcnlCarrierOption = yfceleCarrierOptions.getElementsByTagName(CommonConstants.CARRIER_OPTION);
						   for(YFCElement yfceleCarrierOption : yfcnlCarrierOption) {
							   
					   Element eleCarrierOption = XMLUtil.createElement(docPromise,CommonConstants.CARRIER_OPTION, null);
					   eleCarrierOption.setAttribute(CommonConstants.SHIPPING_CHARGE, yfceleCarrierOption.getAttribute(CommonConstants.SHIPPING_CHARGE)); 
					   eleCarrierOption.setAttribute(CommonConstants.CURRENCY, yfceleCarrierOption.getAttribute(CommonConstants.CURRENCY)); 
						  eleCarrierOption.setAttribute(CommonConstants.SCAC, yfceleCarrierOption.getAttribute(CommonConstants.SCAC)); 
						  eleCarrierOption.setAttribute(CommonConstants.CARRIER_SERVICE_CODE, yfceleCarrierOption.getAttribute(CommonConstants.CARRIER_SERVICE_CODE)); 
						  eleCarrierOption.setAttribute(CommonConstants.ESTIMATED_DELIVERY_DATE, yfceleCarrierOption.getAttribute(CommonConstants.EXPECTED_DELIVERY_DATE)); 
						  XMLUtil.appendChild(eleCarrierOptions, eleCarrierOption);
						  
					   }
						   XMLUtil.appendChild(elePromiseShipment, eleCarrierOptions);
					   }
					   XMLUtil.appendChild(elePromiseShipment, elePromiseShipmentLines);
					   XMLUtil.appendChild(elePromiseShipments, elePromiseShipment);
	    	   }//yfcNLPromiseSHipment
	    	 
		    	//adding unavailable lines
	    	
	    	   YFCElement yfceleSugestedOPtion = yfceleChildPromise.getChildElement(CommonConstants.SUGGESTED_OPTION);
	    	   YFCElement yfceleOPtion = yfceleSugestedOPtion.getChildElement(CommonConstants.OPTION);
	    	   eleOption.setAttribute(CommonConstants.FIRST_DATE, yfceleOPtion.getAttribute(CommonConstants.FIRST_DATE));
	    	   eleOption.setAttribute(CommonConstants.LAST_DATE, yfceleOPtion.getAttribute(CommonConstants.LAST_DATE));
			    YFCElement yfceleUnavailableLines = yfceleSugestedOPtion.getChildElement(CommonConstants.UNAVAILABLE_LINES);
			    if(!YFCElement.isNull(yfceleUnavailableLines)) {
			    	 Element eleUnavailableLines = XMLUtil.createElement(docPromise,CommonConstants.UNAVAILABLE_LINES, null);
			    	 YFCNodeList<YFCElement> nlUnavailableLine = yfceleUnavailableLines.getElementsByTagName(CommonConstants.UNAVAILABLE_LINE);
			    	 for(YFCElement yfceleUnavailableLine : nlUnavailableLine) {
			    		 Element eleUnavailableLine = XMLUtil.createElement(docPromise,CommonConstants.UNAVAILABLE_LINE, null);
			    		 eleUnavailableLine.setAttribute(CommonConstants.ITEM_ID, yfceleUnavailableLine.getAttribute(CommonConstants.ITEM_ID));
			    		 eleUnavailableLine.setAttribute(CommonConstants.PRODUCT_CLASS, yfceleUnavailableLine.getAttribute(CommonConstants.PRODUCT_CLASS));
			    		 eleUnavailableLine.setAttribute(CommonConstants.UNIT_OF_MEASURE, yfceleUnavailableLine.getAttribute(CommonConstants.UNIT_OF_MEASURE));
			    		 eleUnavailableLine.setAttribute(CommonConstants.UNAVAILABLE_REASON, yfceleUnavailableLine.getAttribute(CommonConstants.UNAVAILABLE_REASON));
			    		 eleUnavailableLine.setAttribute(CommonConstants.REQUIRED_QTY, yfceleUnavailableLine.getAttribute(CommonConstants.REQUIRED_QTY));
			    		 eleUnavailableLine.setAttribute(CommonConstants.ASSIGNED_QTY, yfceleUnavailableLine.getAttribute(CommonConstants.ASSIGNED_QTY));
			    		 XMLUtil.appendChild(eleUnavailableLines, eleUnavailableLine);
			    		 
			    	 }
			    	 XMLUtil.appendChild(eleOption, eleUnavailableLines);
			    	 //XMLUtil.appendChild(docPromise.getDocumentElement(), docInput.getDocumentElement());
			    }
	    	  
	    	}//Options
	    	
	    	//adding unavailable lines when there are no options generated by the system and only unavailable lines are in output
	    	 if((yfceleOptions == null) && ((XMLUtil.getElementByXPath(docPromise, CommonConstants.XPATH_UNAVAILABLE_LINES)) == null)){
	 	   YFCElement yfceleSugestedOPtion = yfceleChildPromise.getChildElement(CommonConstants.SUGGESTED_OPTION);
		    YFCElement yfceleUnavailableLines = yfceleSugestedOPtion.getChildElement(CommonConstants.UNAVAILABLE_LINES);
		    if(!YFCElement.isNull(yfceleUnavailableLines)) {
		    	 Element eleUnavailableLines = XMLUtil.createElement(docPromise,CommonConstants.UNAVAILABLE_LINES, null);
		    	 Element eleUnavailableLinesOption = XMLUtil.createElement(docPromise,CommonConstants.OPTION, null);
		    	 YFCNodeList<YFCElement> nlUnavailableLine = yfceleUnavailableLines.getElementsByTagName(CommonConstants.UNAVAILABLE_LINE);
		    	 for(YFCElement yfceleUnavailableLine : nlUnavailableLine) {
		    		 Element eleUnavailableLine = XMLUtil.createElement(docPromise,CommonConstants.UNAVAILABLE_LINE, null);
		    		 eleUnavailableLine.setAttribute(CommonConstants.ITEM_ID, yfceleUnavailableLine.getAttribute(CommonConstants.ITEM_ID));
		    		 eleUnavailableLine.setAttribute(CommonConstants.PRODUCT_CLASS, yfceleUnavailableLine.getAttribute(CommonConstants.PRODUCT_CLASS));
		    		 eleUnavailableLine.setAttribute(CommonConstants.UNIT_OF_MEASURE, yfceleUnavailableLine.getAttribute(CommonConstants.UNIT_OF_MEASURE));
		    		 eleUnavailableLine.setAttribute(CommonConstants.UNAVAILABLE_REASON, yfceleUnavailableLine.getAttribute(CommonConstants.UNAVAILABLE_REASON));
		    		 eleUnavailableLine.setAttribute(CommonConstants.REQUIRED_QTY, yfceleUnavailableLine.getAttribute(CommonConstants.REQUIRED_QTY));
		    		 eleUnavailableLine.setAttribute(CommonConstants.ASSIGNED_QTY, yfceleUnavailableLine.getAttribute(CommonConstants.ASSIGNED_QTY));
		    		 XMLUtil.appendChild(eleUnavailableLines, eleUnavailableLine);
		    		 
		    	 }
		    	 XMLUtil.appendChild(eleUnavailableLinesOption, eleUnavailableLines);
		    	 XMLUtil.appendChild(eleOptions, eleUnavailableLinesOption);
		    }
		    }
	    	 XMLUtil.appendChild(elePromiseOutput, eleOptions);
		    	 }//API
	   //System.out.println("FInal docWrapperClass" + XMLUtil.getXMLString(docPromise));
	    log.verbose("FInal docWrapperClass" + XMLUtil.getXMLString(docPromise));
		log.endTimer("prepareResponseForSalesForce");
	    	 }
		
	catch (Exception e) {
		e.printStackTrace();
		log.verbose("InSide Exception",e);
		log.error(e.getMessage());
		throw new YFSException("logger:inside wrapper class");
	}
		return docPromise;
		
}
		
	/*public static void main(String []args) throws Exception {
		FulfillmentOptionsWrapper fOV = new FulfillmentOptionsWrapper();
		
		fOV.prepareResponseForSalesForce();
		
	}*/

}
