package com.fp.oms.api.inventory.fulfillmentOptions;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.fp.oms.util.CommonConstants;
import com.fp.oms.util.CommonOrderUtils;
import com.infosys.es.scm.coe.util.xml.XMLUtil;
import com.yantra.interop.japi.YIFApi;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

import sun.rmi.runtime.Log;

/**
 * This class to provide fulfillment option for an order to the end customer. 
 * @version 1.1 : Made following changes as per review comments 
 * 					- Added all the constants in the CommonConstants.java
 * 					- Moved sales force response to separate class
 *                  - Added version in the header comment for tracking
 *                  - Moved the output under /Option tag of the output xml
 *                  - Added getCommonCodeForValidation method for retrieving commonCode.	
 *          1.2: Made following changes in the code as per latest requirements
 *          		- added isCommercialAddress ,a new identifier in the sales force response and delivery option input.OMS is getting this attribute from Salesforce under shipToAddress.
 *                  - commented out getItemList call, as per the latest updates SalesForce fill be sending the item dimensions in the input
 *          1.3: Removed all sysout statements and replaced them with Verbose
 *               As part of sanity testing found that the DisplayFrightCharges was not reflecting correct value,so fix the code for that      		
 */
public class FulfillmentOptionsValidations {
	private static YFCLogCategory log = YFCLogCategory.instance(FulfillmentOptionsValidations.class);
	
	/**
	 * Method to get shipment group . Groups are created on the basis of interactions.
	 * @param yfcinxmlElem : root element of the input to this class
	 * @return map of interactionNo(key) and list of promiselines(values)
	 */
	private Map<String, ArrayList<YFCElement>> getShipmentGroup(YFCElement yfcinxmlElem){
		log.verbose("Inside getShipmentGroup : start");
		YFCElement eleOutput = yfcinxmlElem.getChildElement(CommonConstants.OUTPUT);
		YFCElement elePromise = eleOutput.getChildElement(CommonConstants.PROMISE);
		YFCElement eleSuggestedOption = elePromise.getChildElement(CommonConstants.SUGGESTED_OPTION);
		YFCElement eleOption = eleSuggestedOption.getChildElement(CommonConstants.OPTION);
		Map<String, ArrayList<YFCElement>> hmShipmentGroup = null;
		if(null != eleOption) {
		YFCElement elePromiseLines = eleOption.getChildElement(CommonConstants.PROMISE_LINES);		
		YFCNodeList<YFCElement> nlPromiseLineList = elePromiseLines.getElementsByTagName(CommonConstants.PROMISE_LINE);		
		
		 hmShipmentGroup = new HashMap<String, ArrayList<YFCElement>>();
		
	    for(YFCElement elePromiseLine : nlPromiseLineList) {
	    	log.verbose("Inside getShipmentGroup : Inside 1st for loop");
	    	YFCNodeList<YFCElement> nlPromiseLineList1 = elePromiseLines.getElementsByTagName(CommonConstants.PROMISE_LINE);
	    	log.verbose("Inside getShipmentGroup :nlPromiseLineList1"+ nlPromiseLineList1);
	    	YFCElement eleAssignments = elePromiseLine.getChildElement(CommonConstants.ASSIGNMENTS);
	    	YFCNodeList<YFCElement> nlAssignment = eleAssignments.getElementsByTagName(CommonConstants.ASSIGNMENT);
	    	
	    	if(nlAssignment !=null) {
	    	for(YFCElement eleAssignment : nlAssignment) {
	    		ArrayList<YFCElement> list = new ArrayList<YFCElement>();
	    		String sInteractionNo = eleAssignment.getAttribute(CommonConstants.INTERACTION_NO);
	    		if(null != sInteractionNo) {
	    	if(!hmShipmentGroup.containsKey(sInteractionNo)) {
	    		log.verbose("Inside getShipmentGroup : Inside if condition for checking key");
	    	for(YFCElement elePromiseLine1 : nlPromiseLineList1) {
	    		YFCElement eleAssignments1 = elePromiseLine1.getChildElement(CommonConstants.ASSIGNMENTS);
	    		YFCNodeList<YFCElement> nlAssignment1 = eleAssignments1.getElementsByTagName(CommonConstants.ASSIGNMENT);
	    		for(YFCElement eleAssignment1 : nlAssignment1) {
	    		if (sInteractionNo.equals(eleAssignment1.getAttribute(CommonConstants.INTERACTION_NO))){
	    			log.verbose("Inside getShipmentGroup : Inside if condition");
	    			list.add(elePromiseLine1);   		
	    		    				    			
	    		}
	    		    	}
	    	
	    	hmShipmentGroup.put(sInteractionNo, list);
	    	}
		}
	    		}
	    		log.verbose("Shipment group :" + hmShipmentGroup.values());
	    	}
	    	//shipment group logic ends
	 }
	    }
		}
	    log.verbose("Inside getShipmentGroup : end");
	   return hmShipmentGroup;
		
	}

	/**
	 * The method creates PromiseShipmentGroups  which will be part of input to TMS as well as response to SalesForce.
	 * @param docTMSInput : The inout coming from salesforce is the input to this method
	 * @param hmShipmentGroup : map of interactionNo(Key) and promiseLines(Value)
	 * @param isTMSWebserviceInput : boolean value to check if method is used for TMS call or for response to SalesForce
	 *                                 Value is True: The output of the method is used as a webservice input.PromiseLines with
	 *                                                 DeliveryMethod as PICK will not be present in output
	 *                                 Value is False: The output of the method is used for creating response to SalesForce.Will contain
	 *                                 					all promiseLines irrespective of DeliveryMethod.							
	 * @return Document containing shipment groups
	 */
	// private Document prepareInputforSalesForceResponse(Document docTMSInput,Map<String, ArrayList<YFCElement>> hmShipmentGroup, boolean isTMSWebserviceInput) {
		 private Document prepareInputforSalesForceResponse(YFSEnvironment env,Document docTMSInput,Map<String, ArrayList<YFCElement>> hmShipmentGroup, boolean isTMSWebserviceInput) {
					
		 log.verbose("Inside prepareInputforSalesForceResponse : start");
		 YFCElement yfcinxmlElem = YFCDocument.getDocumentFor(docTMSInput).getDocumentElement();
		 YFCElement elePromiseLines = yfcinxmlElem.getChildElement(CommonConstants.PROMISE_LINES);
		
		 YFCElement eleOutput = yfcinxmlElem.getChildElement(CommonConstants.OUTPUT);
		 YFCElement elePromise = eleOutput.getChildElement(CommonConstants.PROMISE);
		 YFCElement eleSuggestedOption = elePromise.getChildElement(CommonConstants.SUGGESTED_OPTION);
		YFCElement eleOption = eleSuggestedOption.getChildElement(CommonConstants.OPTION);  
		
		try {
			  
			Element elePromiseRoot = XMLUtil.getElementByXPath(docTMSInput, CommonConstants.XPATH_PROMISE_INPUT);
		    Element elePromiseShipments = XMLUtil.createElement(docTMSInput,CommonConstants.PROMISE_SHIPMENTS,null);
		   
		    XMLUtil.setAttribute(elePromiseShipments,CommonConstants.ALLOCATION_RULE_ID, elePromise.getAttribute(CommonConstants.ALLOCATION_RULE_ID));
		    XMLUtil.setAttribute(elePromiseShipments,CommonConstants.ENTERPRISE_CODE, elePromise.getAttribute(CommonConstants.ENTERPRISE_CODE));
		    //XMLUtil.appendChild(eleOption1, elePromiseShipments);
		    log.verbose("Inside prepareInputforSalesForceResponse : map iteration start");
		    //forming shipment group or promiseShipments
		    for (String sKey : hmShipmentGroup.keySet()) {
		    	 log.verbose("Inside prepareInputforSalesForceResponse : looping through shipment group: forming promiseShipment");
		    	ArrayList<YFCElement>al = hmShipmentGroup.get(sKey);
		    	Element elePromiseShipment = XMLUtil.createElement(docTMSInput,CommonConstants.PROMISE_SHIPMENT, null);
			    XMLUtil.setAttribute(elePromiseShipment,CommonConstants.GROUP_ID, sKey);//will be set as interaction no
			    
			    Element elePromiseShipmentLines = XMLUtil.createElement(docTMSInput,CommonConstants.PROMISE_SHIPMENT_LINES, null);
			    XMLUtil.appendChild(elePromiseShipment, elePromiseShipmentLines);
			    YFCElement eleShipToAddress = null;
			    String sUnitCost = null;
			    String sShipmentMode = null;
			    //adding promise shipment lines
			    for (int i = 0; i < al.size(); i++) {
				   log.verbose("Inside prepareInputforSalesForceResponse : looping through promiselines");
				   YFCElement elePromiseLine = al.get(i);
				   eleShipToAddress = elePromiseLine.getChildElement(CommonConstants.SHIP_TO_ADDRESS);
				   XMLUtil.setAttribute(elePromiseShipment,CommonConstants.DELIVERY_METHOD, elePromiseLine.getAttribute(CommonConstants.DELIVERY_METHOD));
				   Element elePromiseShipmentLine = XMLUtil.createElement(docTMSInput,CommonConstants.PROMISE_SHIPMENT_LINE, null);
				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_ID, elePromiseLine.getAttribute(CommonConstants.ITEM_ID));
				   String sItemID = elePromiseLine.getAttribute(CommonConstants.ITEM_ID);
				   //Getting PromiseLine from Promise/PromiseLines
				   YFCIterable<YFCElement> yfcNLPromiseLine = elePromiseLines.getChildren();
				  //Getting ShipToAddress, shipmentMode and UnitCost from Promise/PromiseLines/Promise ,coming as part of request from SalesForce
				   for(YFCElement yfcelePromiseLine : yfcNLPromiseLine) {
					   log.verbose("Inside prepareInputforSalesForceResponse : looping through promiselines");
					  if((yfcelePromiseLine.getAttribute(CommonConstants.ITEM_ID)).equals(sItemID)){
						 /// eleShipToAddress = yfcelePromiseLine.getChildElement(CommonConstants.SHIP_TO_ADDRESS);
						  sUnitCost = yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_COST);
						  sShipmentMode = yfcelePromiseLine.getAttribute(CommonConstants.SHIPMENT_MODE);
						  if(null!= yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_HEIGHT)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_HEIGHT, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_HEIGHT));
						   } else {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_HEIGHT, "");
						   }
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_HEIGHT_UOM)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_HEIGHT_UOM, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_HEIGHT_UOM));
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_HEIGHT_UOM, "");
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_LENGTH)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_LENGTH, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_LENGTH));
						   }  else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_LENGTH, ""); 
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_LENGTH_UOM)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_LENGTH_UOM, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_LENGTH_UOM));   				   
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_LENGTH_UOM, "");
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_VOLUME)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_VOLUME, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_VOLUME));
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_VOLUME, "");
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_VOLUME_UOM)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_VOLUME_UOM, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_VOLUME_UOM));
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_VOLUME_UOM, "");
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WEIGHT)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WEIGHT, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WEIGHT));
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WEIGHT, "");
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WEIGHT_UOM)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WEIGHT_UOM, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WEIGHT_UOM));
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WEIGHT_UOM, "");
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WIDTH)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WIDTH,yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WIDTH));
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WIDTH, "");
						   if(null!=yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WIDTH_UOM)) {
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WIDTH_UOM, yfcelePromiseLine.getAttribute(CommonConstants.ITEM_UNIT_WIDTH_UOM));
						   }else
							   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WIDTH_UOM, "");
						   
						  break;
					  }
				   }
				   /*
				    * Getting item details like weight, height etc by calling getItemList.
				    */
				/*   Document outDoc = null;
				   String sDeliveryMethod = elePromiseLine.getAttribute(CommonConstants.DELIVERY_METHOD);
				   				if((null != sDeliveryMethod) && ((CommonConstants.DELIVERY_METHOD_SHP)).equals(sDeliveryMethod)) {  
				   						 outDoc = getItemDetailsForTMS(elePromiseLine,env);
				   					  Element elePrimaryInformation = XMLUtil.getElementByXPath(outDoc, CommonConstants.PRIMARY_INFORMATION_XPATH);
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_HEIGHT, elePrimaryInformation.getAttribute(CommonConstants.UNIT_HEIGHT));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_HEIGHT_UOM, elePrimaryInformation.getAttribute(CommonConstants.UNIT_HEIGHT_UOM));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_LENGTH, elePrimaryInformation.getAttribute(CommonConstants.UNIT_LENGTH));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_LENGTH_UOM, elePrimaryInformation.getAttribute(CommonConstants.UNIT_LENGTH_UOM));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_VOLUME, elePrimaryInformation.getAttribute(CommonConstants.UNIT_VOLUME));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_VOLUME_UOM, elePrimaryInformation.getAttribute(CommonConstants.UNIT_VOLUME_UOM));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WEIGHT, elePrimaryInformation.getAttribute(CommonConstants.UNIT_WEIGHT));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WEIGHT_UOM, elePrimaryInformation.getAttribute(CommonConstants.UNIT_WEIGHT_UOM));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WIDTH,elePrimaryInformation.getAttribute(CommonConstants.UNIT_WIDTH));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_WIDTH_UOM, elePrimaryInformation.getAttribute(CommonConstants.UNIT_WIDTH_UOM));
				   				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.IS_PARCEL_SHIPPING_ALLOWED, elePrimaryInformation.getAttribute(CommonConstants.IS_PARCEL_SHIPPING_ALLOWED));
				   		}*/
				      
				 
				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.PRODUCT_CLASS, elePromiseLine.getAttribute(CommonConstants.PRODUCT_CLASS));
				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.UNIT_OF_MEASURE, elePromiseLine.getAttribute(CommonConstants.UNIT_OF_MEASURE));
				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.DELIVERY_METHOD,elePromiseLine.getAttribute(CommonConstants.DELIVERY_METHOD));	
				  //Assuming SalesForce will be sending in Promise/PromiseLines/PromiseLine
				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.ITEM_UNIT_COST,sUnitCost);
				  // System.out.println("ShipMode__________" + sShipmentMode);
				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.SHIPMENT_MODE,sShipmentMode);
				   XMLUtil.setAttribute(elePromiseShipmentLine,CommonConstants.FULFILLMENT_TYPE,elePromiseLine.getAttribute(CommonConstants.FULFILLMENT_TYPE));
			
				   XMLUtil.appendChild(elePromiseShipmentLines, elePromiseShipmentLine);			  
				   
				   //creating Assignments
				   YFCElement yfcAssignments = elePromiseLine.getChildElement(CommonConstants.ASSIGNMENTS);
				   
				   YFCIterable<YFCElement> yfcNLAssignment = yfcAssignments.getChildren();
				   Element eleAssignments = XMLUtil.createElement(docTMSInput,CommonConstants.ASSIGNMENTS, null);
				   for(YFCElement yfcnAssignment : yfcNLAssignment) {
				   String sIntercationNo = yfcnAssignment.getAttribute(CommonConstants.INTERACTION_NO);
				   if(sIntercationNo != null) {
				   if(sIntercationNo.equals(sKey)) {
				   Element eleAssignment = XMLUtil.createElement(docTMSInput,CommonConstants.ASSIGNMENT, null);
				   eleAssignment.setAttribute(CommonConstants.DELIVERY_DATE, yfcnAssignment.getAttribute(CommonConstants.DELIVERY_DATE) );
				   eleAssignment.setAttribute(CommonConstants.INTERACTION_NO, yfcnAssignment.getAttribute(CommonConstants.INTERACTION_NO));
				   eleAssignment.setAttribute(CommonConstants.PROCURED_QTY, yfcnAssignment.getAttribute(CommonConstants.PROCURED_QTY));
				   eleAssignment.setAttribute(CommonConstants.QUANTITY, yfcnAssignment.getAttribute(CommonConstants.QUANTITY));
				   eleAssignment.setAttribute(CommonConstants.SHIP_DATE, yfcnAssignment.getAttribute(CommonConstants.SHIP_DATE));
				   eleAssignment.setAttribute(CommonConstants.EMPTY_ASSIGNMENT_REASON, yfcnAssignment.getAttribute(CommonConstants.EMPTY_ASSIGNMENT_REASON));
				   eleAssignment.setAttribute(CommonConstants.SHIP_NODE, yfcnAssignment.getAttribute(CommonConstants.SHIP_NODE));
				   elePromiseShipment.setAttribute(CommonConstants.SHIP_NODE, yfcnAssignment.getAttribute(CommonConstants.SHIP_NODE));
				   elePromiseShipment.setAttribute(CommonConstants.SHIP_DATE, yfcnAssignment.getAttribute(CommonConstants.SHIP_DATE));
				   elePromiseShipmentLine.setAttribute(CommonConstants.AVAILABLE_QUANTITY, yfcnAssignment.getAttribute(CommonConstants.QUANTITY));
				  
				   /*
				    * Only if delivery method is customer delivery or local delivery we have Procurement Details.
				    * Putting a check on delivery method
				    */
				   if(!(CommonConstants.DELIVERY_METHOD_SHP.equals(elePromiseShipmentLine.getAttribute(CommonConstants.DELIVERY_METHOD)))){
			       YFCElement yfcProcurements = yfcnAssignment.getChildElement(CommonConstants.PROCUREMENTS);
				   YFCIterable<YFCElement> yfcnProcurement = yfcProcurements.getChildren();
				   Element eleProcurements = XMLUtil.createElement(docTMSInput,CommonConstants.PROCUREMENTS, null);
				   if(null != yfcnProcurement) {
					   
				   for(YFCElement yfcProcurement:yfcnProcurement ) {
				   Element eleProcurement = XMLUtil.createElement(docTMSInput,CommonConstants.PROCUREMENT, null);
				   
				   eleProcurement.setAttribute(CommonConstants.PROCURE_FROM_NODE, yfcProcurement.getAttribute(CommonConstants.PROCURE_FROM_NODE));
				   eleProcurement.setAttribute(CommonConstants.PRODUCT_AVAIL_DATE, yfcProcurement.getAttribute(CommonConstants.PRODUCT_AVAIL_DATE));
				   eleProcurement.setAttribute(CommonConstants.QUANTITY, yfcProcurement.getAttribute(CommonConstants.QUANTITY));
				   eleProcurement.setAttribute(CommonConstants.SHIP_DATE, yfcProcurement.getAttribute(CommonConstants.SHIP_DATE));
				   elePromiseShipment.setAttribute(CommonConstants.PROCURE_FROM_NODE, yfcProcurement.getAttribute(CommonConstants.PROCURE_FROM_NODE));
				   
				   XMLUtil.appendChild(eleProcurements, eleProcurement);
				   }
				   XMLUtil.appendChild(eleAssignment, eleProcurements);
				   }
				   XMLUtil.appendChild(eleAssignments, eleAssignment);
				   XMLUtil.appendChild(elePromiseShipmentLine, eleAssignments);
				   }
				   XMLUtil.appendChild(eleAssignments, eleAssignment);
				   XMLUtil.appendChild(elePromiseShipmentLine, eleAssignments);
				   }
				   }
				   }
				  // System.out.println("docTMSInput" + XMLUtil.getXMLString(docTMSInput));
			    }
				 
			    
			   
			      
				   log.verbose("Inside prepareInputforSalesForceResponse : appending ship to address");
			    		Element eleShipToAddressTeamp = XMLUtil.createElement(docTMSInput,CommonConstants.SHIP_TO_ADDRESS, null);
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.ADDRESS_LINE_1, eleShipToAddress.getAttribute(CommonConstants.ADDRESS_LINE_1) );
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.ADDRESS_LINE_2, eleShipToAddress.getAttribute(CommonConstants.ADDRESS_LINE_2) );
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.ADDRESS_LINE_3, eleShipToAddress.getAttribute(CommonConstants.ADDRESS_LINE_3));
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.CITY, eleShipToAddress.getAttribute(CommonConstants.CITY));
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.COUNTRY, eleShipToAddress.getAttribute(CommonConstants.COUNTRY));
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.STATE, eleShipToAddress.getAttribute(CommonConstants.STATE));
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.ZIP_CODE, eleShipToAddress.getAttribute(CommonConstants.ZIP_CODE));
			    		eleShipToAddressTeamp.setAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS, eleShipToAddress.getAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS));
			    		XMLUtil.appendChild(elePromiseShipment, eleShipToAddressTeamp);
			    		 //If deliveryMethod is PICK then TMS webservice call will not be made. So putting a check for the same.
						   if(false) {
							   if(elePromiseShipment.getAttribute(CommonConstants.DELIVERY_METHOD).equals(CommonConstants.DELIVERY_METHOD_SHP)) {
						         XMLUtil.appendChild(elePromiseShipments, elePromiseShipment);
						   } 
						   }else {
							   
							   XMLUtil.appendChild(elePromiseShipments, elePromiseShipment);
							   
						   }
			    
		    	}
		    log.verbose("Inside prepareInputforSalesForceResponse : appending option in the response to slaesforce");
		      Element eleOptions = XMLUtil.createElement(docTMSInput,CommonConstants.OPTIONS,null);
			   Element eleOptionPromiseShipment = XMLUtil.createElement(docTMSInput,CommonConstants.OPTION,null);
			   eleOptionPromiseShipment.setAttribute(CommonConstants.ALLOCATION_RULE_ID, elePromise.getAttribute(CommonConstants.ALLOCATION_RULE_ID));
			   eleOptionPromiseShipment.setAttribute(CommonConstants.HAS_ANY_UNAVAILABLE_QTY, eleOption.getAttribute(CommonConstants.HAS_ANY_UNAVAILABLE_QTY));
			   eleOptionPromiseShipment.setAttribute(CommonConstants.TOTAL_SHIPMENTS, eleOption.getAttribute(CommonConstants.TOTAL_SHIPMENTS));			   
			   XMLUtil.appendChild(eleOptionPromiseShipment, elePromiseShipments);
			   XMLUtil.appendChild(eleOptions, eleOptionPromiseShipment);
		       XMLUtil.appendChild(elePromiseRoot, eleOptions);
		       
		     log.verbose("Inside prepareInputforSalesForceResponse" + XMLUtil.getXMLString(docTMSInput));
		}catch(Exception e){
			log.verbose("Inside prepareInputforSalesForceResponse : exception");
			//System.out.println(e.getStackTrace());
			 
		}
		 log.verbose("Inside prepareInputforSalesForceResponse : end");
		    return docTMSInput;
	}

	/**
	 * Method to call getItemList API 
	 * @param elePromiseLine PromiseLine for which items details are required
	 * @return Document : Details of the item 
	 */
	private Document getItemDetailsForTMS(YFCElement elePromiseLine,YFSEnvironment env ) {
	//private Document getItemDetailsForTMS(YFCElement elePromiseLine){
		 log.verbose("Inside getItemDetailsForTMS : start");
		 Document inputDoc = null;
		 Document outDoc = null;
		try {
		  inputDoc = XMLUtil.createDocument(CommonConstants.ITEM);
		   Element eleInputItemList = inputDoc.getDocumentElement();
		   eleInputItemList.setAttribute(CommonConstants.ITEM_ID, elePromiseLine.getAttribute(CommonConstants.ITEM_ID));
		   eleInputItemList.setAttribute(CommonConstants.UOM, elePromiseLine.getAttribute(CommonConstants.UNIT_OF_MEASURE));
		  //call getItemList
		   //InputStream is = new FileInputStream("getItemList_output.xml");
		  YIFApi oApi = YIFClientFactory.getInstance().getLocalApi();   
		 outDoc = oApi.invoke(env, CommonConstants.GET_ITEM_LIST_API, inputDoc);
		 outDoc= CommonOrderUtils.invokeApiWithTemplate(env, CommonConstants.GET_ITEM_LIST_API, inputDoc, CommonConstants.TEMPLATE_GET_ITEM_LIST);
		//   outDoc = XMLUtil.getDocument(is);
		   log.verbose("Inside getItemDetailsForTMS : end" + XMLUtil.getXMLString(outDoc));
		} catch(Exception e) {
		//	System.out.println(e.getStackTrace());
			e.printStackTrace();
			log.verbose("InSide Exception",e);
			log.error(e.getMessage());
			throw new YFSException("logger: insidegetItemlist");
		}
		return outDoc;
	}
	
	/**
	 * Method to call getItemList API 
	 * @param elePromiseLine PromiseLine for which items details are required
	 * @return Document : Details of the item 
	 */
	private Document getCommonCodeForValidation(YFSEnvironment env) {
	//	private Document getCommonCodeForValidation() {
			 log.verbose("Inside getCommonCodeForValidation : start" );
			
		 Document inputDoc = null;
		 Document outDoc = null;
		try {
		  inputDoc = XMLUtil.createDocument(CommonConstants.COMMON_CODE);
		   Element eleInputCommonCode = inputDoc.getDocumentElement();
		   eleInputCommonCode.setAttribute(CommonConstants.CODE_TYPE, CommonConstants.FULFILL_OPT_VALIDATION_CODE_TYPE);
		  //call getCommonCode
		   YIFApi oApi = YIFClientFactory.getInstance().getLocalApi();   
		   outDoc = CommonOrderUtils.invokeAPI(env, CommonConstants.GET_COMMON_CODE_LIST_API, inputDoc);
		  // InputStream is = new FileInputStream("getCommonCodeList_output.xml");
		  // outDoc = XMLUtil.getDocument(is);
		   log.verbose("Inside getCommonCodeForValidation : end" );
		} catch(Exception e) {
			//System.out.println(e.getStackTrace());
			e.printStackTrace();
			 log.verbose("Inside getCommonCodeForValidation : exception" );
			log.error(e.getMessage());
			throw new YFSException("logger:inside getCommonCode");
		}
		return outDoc;
	}
		
	/**
	 * Method to create webservice request to TMS. 
	 * @param inputElement input to the java class
	 * @return Document TMS webservice request
	 */

	private Document prepareInputForTMS(YFCElement inputElement) {
		 log.verbose("Inside prepareInputForTMS : start" );
		Document docWSRequest = null;
		String isParcelIneligible = "N";
		try {
		 docWSRequest = XMLUtil.createDocument(CommonConstants.PROMISE_SHIPMENTS);
		 
		Element eleWSRequest = docWSRequest.getDocumentElement();
		double dItemWeight = 0.0;
		double dItemVolume = 0.0;
		YFCElement eleOutput = inputElement.getChildElement(CommonConstants.OUTPUT);
		//YFCElement eleShipToAddressOutput = inputElement.getChildElement(CommonConstants.SHIP_TO_ADDRESS);
	//	String sIsCommercialAddress = eleShipToAddressOutput.getAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS);
		YFCElement elePromise = eleOutput.getChildElement(CommonConstants.PROMISE);
		YFCElement eleOptions = elePromise.getChildElement(CommonConstants.OPTIONS);
		YFCElement eleOption = eleOptions.getChildElement(CommonConstants.OPTION);
		YFCElement yfcelePromiseShipments = eleOption.getChildElement(CommonConstants.PROMISE_SHIPMENTS);
		//boolean isShimentModeLTL = false;
		boolean isProcurementAvailable = false;
		 String sShipmentMode = null;
		 boolean sParcelShippingAllowed =true;
		 boolean bParcelIneligible = true;
		eleWSRequest.setAttribute("EnterpriseCode", yfcelePromiseShipments.getAttribute(CommonConstants.ENTERPRISE_CODE));
		eleWSRequest.setAttribute("OrganizationCode", elePromise.getAttribute(CommonConstants.ORGANIZATION_CODE));
		 //YFCElement yfcelePromiseShipments = elePromiseShipments.getChildElement(CommonConstants.PROMISE_SHIPMENTS);
		  YFCIterable<YFCElement>  nlPromiseShipment = yfcelePromiseShipments.getChildren();
		  for(YFCElement yfcelePromiseShipment : nlPromiseShipment) {
			 // if((CommonConstants.DELIVERY_METHOD_SHP).equals(yfcelePromiseShipment.getAttribute(CommonConstants.DELIVERY_METHOD))) {
			  if((CommonConstants.DELIVERY_METHOD_SHP).equals(yfcelePromiseShipment.getAttribute(CommonConstants.DELIVERY_METHOD)) || 
					  (((CommonConstants.DELIVERY_METHOD_PICK).equals(yfcelePromiseShipment.getAttribute(CommonConstants.DELIVERY_METHOD)))
							  && ((null!=yfcelePromiseShipment.getAttribute(CommonConstants.PROCURE_FROM_NODE))
							  &&(((yfcelePromiseShipment.getAttribute(CommonConstants.PROCURE_FROM_NODE))!=""))))){
              
			  Element elePromiseShipment = XMLUtil.createElement(docWSRequest, CommonConstants.PROMISE_SHIPMENT, null);
			  elePromiseShipment.setAttribute(CommonConstants.GROUP_ID, yfcelePromiseShipment.getAttribute(CommonConstants.GROUP_ID));	
			  elePromiseShipment.setAttribute(CommonConstants.SHIP_NODE, yfcelePromiseShipment.getAttribute(CommonConstants.SHIP_NODE));			  
			  elePromiseShipment.setAttribute(CommonConstants.ENTERPRISE_CODE, yfcelePromiseShipments.getAttribute(CommonConstants.ENTERPRISE_CODE));
			  elePromiseShipment.setAttribute(CommonConstants.ORGANIZATION_CODE, elePromise.getAttribute(CommonConstants.ORGANIZATION_CODE));
			  
			  elePromiseShipment.setAttribute(CommonConstants.EARLIEST_SHIP_DATE, yfcelePromiseShipment.getAttribute(CommonConstants.SHIP_DATE));
			  ZonedDateTime dateTime = ZonedDateTime.parse(elePromiseShipment.getAttribute(CommonConstants.EARLIEST_SHIP_DATE));
			  
			  ZonedDateTime zdtLatestShipDate = dateTime.plusHours(Integer.parseInt(CommonConstants.LATEST_SHIP_DATE_OFFSET));			  
			  elePromiseShipment.setAttribute(CommonConstants.LATEST_SHIP_DATE,  zdtLatestShipDate.toString());
			  
			  ZonedDateTime zdtEarliestDeliveryDate = dateTime.plusMinutes(Integer.parseInt(CommonConstants.EARLIEST_DELIVERY_DATE_OFFSET));			  
			  elePromiseShipment.setAttribute(CommonConstants.EARLIEST_DELIVERY_DATE,  zdtEarliestDeliveryDate.toString());
			  
			  ZonedDateTime zdtLatestDeliveryDate = dateTime.plusDays(Integer.parseInt(CommonConstants.LATEST_DELIVERY_DATE_OFFSET));			  
			  elePromiseShipment.setAttribute(CommonConstants.LATEST_DELIVERY_DATE,  zdtLatestDeliveryDate.toString());
			  
			 			  
			  YFCElement yfcelePromiseShipmentLines = yfcelePromiseShipment.getChildElement(CommonConstants.PROMISE_SHIPMENT_LINES);
			  YFCIterable<YFCElement> yfcnlPromiseShipmentLine = yfcelePromiseShipmentLines.getChildren();
			  for(YFCElement yfcelePromiseShipmentLine : yfcnlPromiseShipmentLine) {
				  if((CommonConstants.DELIVERY_METHOD_SHP).equals(yfcelePromiseShipmentLine.getAttribute(CommonConstants.DELIVERY_METHOD))){
				  double weight =  yfcelePromiseShipmentLine.getDoubleAttribute(CommonConstants.ITEM_UNIT_WEIGHT);
				  double quantity = yfcelePromiseShipmentLine.getDoubleAttribute(CommonConstants.AVAILABLE_QUANTITY);
				  dItemWeight = dItemWeight + (weight * quantity);
				  // double volume =  yfcelePromiseShipmentLine.getDoubleAttribute(CommonConstants.ITEM_UNIT_VOLUME);
				  //dItemVolume = dItemVolume + (volume * quantity);
				  
				  sShipmentMode = yfcelePromiseShipmentLine.getAttribute(CommonConstants.SHIPMENT_MODE);
				  
				  /*if(sShipmentMode.equals(CommonConstants.SHIPMENT_MODE_LTL)) {
		    			 isShimentModeLTL = true;
		    		 }*/
				  if(sShipmentMode != ""){
					  if((!(sShipmentMode.equals(CommonConstants.SHIPMENT_MODE_PARCEL))) && (bParcelIneligible)){
					  isParcelIneligible = "Y";
					  bParcelIneligible = false;
					  }
				  }/*else if(((yfcelePromiseShipmentLine.getAttribute(CommonConstants.IS_PARCEL_SHIPPING_ALLOWED)).equals("N")) && (isProcurementAvailable)){
					  isParcelIneligible = "Y";
					  isProcurementAvailable = false;
				  }*/
				  
				  
			  }
			  }
			 elePromiseShipment.setAttribute(CommonConstants.IS_PARCEL_INELIGIBLE, isParcelIneligible);
			/*  if (isShimentModeLTL) {
				  elePromiseShipment.setAttribute("ShipmentMode", sShipmentMode);
			  } else {
				  elePromiseShipment.setAttribute("ShipmentMode", CommonConstants.SHIPMENT_MODE_PARCEL); 
			  }
			  */
			  
			  if(((CommonConstants.DELIVERY_METHOD_PICK).equals(yfcelePromiseShipment.getAttribute(CommonConstants.DELIVERY_METHOD)))
					  && ((null!=yfcelePromiseShipment.getAttribute(CommonConstants.PROCURE_FROM_NODE))&&(((yfcelePromiseShipment.getAttribute(CommonConstants.PROCURE_FROM_NODE))!="")))){				
				  isProcurementAvailable = true;
				  elePromiseShipment.setAttribute(CommonConstants.WEIGHT, CommonConstants.PICK_WEIGHT_FOR_TMS);
				  
			  } else {
				  elePromiseShipment.setAttribute(CommonConstants.WEIGHT, String.valueOf(dItemWeight));
			  }	
			  //elePromiseShipment.setAttribute(CommonConstants.VOLUME, String.valueOf(dItemVolume));
			  //System.out.println("isProcurementAvailable" + isProcurementAvailable);
			  dItemWeight = 0.0;
			  dItemVolume = 0.0;
			  Element eleShipToAddress = XMLUtil.createElement(docWSRequest, CommonConstants.SHIP_TO_ADDRESS, null);
			  YFCElement yfceleShipToAddress = yfcelePromiseShipment.getChildElement(CommonConstants.SHIP_TO_ADDRESS);
			  
			  if(isProcurementAvailable) {
				  	eleShipToAddress.setAttribute(CommonConstants.SHIP_TO_LOCATION, yfcelePromiseShipment.getAttribute(CommonConstants.SHIP_NODE));
			  		eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_1, "");
			  		eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_2, "");
			  		eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_3, "");
			  		eleShipToAddress.setAttribute(CommonConstants.CITY, "");
			  		eleShipToAddress.setAttribute(CommonConstants.COUNTRY, "");
			  		eleShipToAddress.setAttribute(CommonConstants.STATE,"");
			  		eleShipToAddress.setAttribute(CommonConstants.ZIPCODE, "");  
			  		//eleShipToAddress.setAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS, sIsCommercialAddress);
			  }	
			  else
				 
			  {
				  eleShipToAddress.setAttribute(CommonConstants.SHIP_TO_LOCATION, CommonConstants.SHIP_TO_LOCATION_DUMMY);			  
				  eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_1, yfceleShipToAddress.getAttribute(CommonConstants.ADDRESS_LINE_1));
				  eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_2, yfceleShipToAddress.getAttribute(CommonConstants.ADDRESS_LINE_2));
				  eleShipToAddress.setAttribute(CommonConstants.ADDRESS_LINE_3, yfceleShipToAddress.getAttribute(CommonConstants.ADDRESS_LINE_3));
				  eleShipToAddress.setAttribute(CommonConstants.CITY, yfceleShipToAddress.getAttribute(CommonConstants.CITY));
				  eleShipToAddress.setAttribute(CommonConstants.COUNTRY, yfceleShipToAddress.getAttribute(CommonConstants.COUNTRY));
				  eleShipToAddress.setAttribute(CommonConstants.STATE, yfceleShipToAddress.getAttribute(CommonConstants.STATE));
				  eleShipToAddress.setAttribute(CommonConstants.ZIPCODE, yfceleShipToAddress.getAttribute(CommonConstants.ZIPCODE)); 
				  eleShipToAddress.setAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS, yfceleShipToAddress.getAttribute(CommonConstants.IS_COMMERCIAL_ADDRESS));
			  }
			  Element eleShipFromAddress = XMLUtil.createElement(docWSRequest, CommonConstants.SHIP_FROM_ADDRESS, null);
			  if(isProcurementAvailable)
				  eleShipFromAddress.setAttribute(CommonConstants.SHIP_FROM_LOCATION, yfcelePromiseShipment.getAttribute(CommonConstants.PROCURE_FROM_NODE));
				  else {
					  eleShipFromAddress.setAttribute(CommonConstants.SHIP_FROM_LOCATION, yfcelePromiseShipment.getAttribute(CommonConstants.SHIP_NODE));
					  
				  }
			  eleShipFromAddress.setAttribute(CommonConstants.ADDRESS_LINE_1, "");
			  eleShipFromAddress.setAttribute(CommonConstants.ADDRESS_LINE_2, "");
			  eleShipFromAddress.setAttribute(CommonConstants.ADDRESS_LINE_3, "");
			  eleShipFromAddress.setAttribute(CommonConstants.CITY, "");
			  eleShipFromAddress.setAttribute(CommonConstants.COUNTRY, "");
			  eleShipFromAddress.setAttribute(CommonConstants.STATE, "");
			  eleShipFromAddress.setAttribute(CommonConstants.ZIPCODE, "");
			  XMLUtil.appendChild(elePromiseShipment, eleShipToAddress);
			  XMLUtil.appendChild(elePromiseShipment, eleShipFromAddress);
			  XMLUtil.appendChild(eleWSRequest, elePromiseShipment);
			 
			  
			  }//ending SHP check
			  log.verbose("Inside prepareInputForTMS : end" + XMLUtil.getXMLString(docWSRequest));
		  } 
		}catch(Exception e) {
			 log.verbose("Inside prepareInputForTMS : exception" );
			//System.out.println(e.getStackTrace());
			e.printStackTrace();
		}
		return docWSRequest;
		
	}
	/**
	 * Method to call TMS .
	 * @param inputElement input to the java class
	 * @param env environment variable
	 * @return Document TMS webservice response
	 */
	private Document callTMSWebservice(YFSEnvironment env, Document inputDoc) {
		 log.verbose("Inside callTMSWebservice : start");
		  Document tmsresponse = null;
		 try {
		 tmsresponse =  CommonOrderUtils.invokeService(env, CommonConstants.TMS_DELIVERY_OPTION_SERVICE, inputDoc);
		
		 }catch(Exception e) {
			 log.verbose("Inside callTMSWebservice : exception");
			 e.printStackTrace();
			 throw new YFCException("Inside callTMSWebservice");
		 }
		 log.verbose("Inside callTMSWebservice : end");
		 return tmsresponse;
		  }
	
	/**
	 * Method to process webservice response from  TMS. 
	 * @param inputElement input to the java class
	 * @return Document TMS webservice response
	 */
	private Document processTMSWebserviceCallResponse(Document inputDoc, Document tmsWSresponse) {
		log.verbose("Inside processTMSWebserviceCallResponse : start");
		  Document doc = tmsWSresponse;
		//	InputStream is = null;
		try {
			//is = new FileInputStream("TMSResponse.xml");
			//doc = XMLUtil.getDocument(is);
		//	System.out.println("documentElement:" + doc.getDocumentElement());
			 Element elePromiseShipments = XMLUtil.getElementByXPath(inputDoc,CommonConstants.XPATH_TMS_OUTPUT_PROMISESHIPMENT);
			  Element eleWSResponsePromiseShipments = doc.getDocumentElement();
			  List<Node> nlWSResponsePromiseShipment = XMLUtil.getElementsByTagName(eleWSResponsePromiseShipments, CommonConstants.PROMISE_SHIPMENT);
			  for(int i=0; i< nlWSResponsePromiseShipment.size(); i++) {
				  Element eleWSResponsePromiseShipment = (Element)nlWSResponsePromiseShipment.get(i);
				  String sWSResponseGroupId = eleWSResponsePromiseShipment.getAttribute(CommonConstants.GROUP_ID);
				  List<Node> nlPromiseShipment = XMLUtil.getElementsByTagName(elePromiseShipments, CommonConstants.PROMISE_SHIPMENT);
			  for(int j=0; j< nlPromiseShipment.size(); j++) {
				  Element elePromiseShipment = (Element)nlPromiseShipment.get(j);
				  String sGroupId = elePromiseShipment.getAttribute(CommonConstants.GROUP_ID);
				  if(sWSResponseGroupId.equals(sGroupId)) {
				 
				  
				  List<Node> nlCarrierOption = XMLUtil.getElementsByTagName(eleWSResponsePromiseShipment, CommonConstants.CARRIER_OPTION);
				  if(nlCarrierOption.size() > 0) {
					  Element eleCarrierOptions = XMLUtil.createElement(inputDoc, CommonConstants.CARRIER_OPTIONS, null);
				  for(int k=0; k< nlCarrierOption.size(); k++) {
					  Element eleRespCarrierOption = (Element)nlCarrierOption.get(k);
					  Element eleCarrierOption = XMLUtil.createElement(inputDoc, CommonConstants.CARRIER_OPTION, null);
					  XMLUtil.copyElement(inputDoc, eleRespCarrierOption, eleCarrierOption);
					  XMLUtil.appendChild(eleCarrierOptions, eleCarrierOption);
				  }
				  XMLUtil.appendChild(elePromiseShipment, eleCarrierOptions);
				  } else {
					  elePromiseShipment.setAttribute(CommonConstants.CARRIER_OPTIONS_NOT_AVAILABLE, "Y");
				  }
				  }
				  
			 
			  }		
			 
			  }
		}catch(Exception e) {
			log.verbose("Inside processTMSWebserviceCallResponse : exception");
			e.printStackTrace();
		}
		log.verbose("Inside processTMSWebserviceCallResponse : end");
		return inputDoc	;	
	}
	/**
	 * In this method following tasks are performed:
	 * 1.Validation of the shimentMode
	 * 2.Grouping logic for forming shipment group required for TMS call
	 * 3.Creating input for the TMS webservice call
	 * 4.Invoking webservice call to TMS
	 * 5.Processing the webservice response
	 * 6.Doing validations on weight and cost
	 * 5.Sending response back to salesforce
	 */
	public Document fulfillmentOptionsGrouping(YFSEnvironment env, Document inputDoc) throws Exception{
	//public void fulfillmentOptionsGrouping(){	
		 log.verbose("Inside fulfillmentOptionsGrouping : start" );
		 log.beginTimer("fulfillmentOptionsGrouping");
	Document doc = inputDoc;
		//InputStream is = null;
		double dItemWeight = 0.0;
		double dItemUnitCost = 0.0;
		Document docSalesForceResponse = null;
		Document dTMRequest = null;
		Document docTMSWebserviceResponse = null;
		Document dTMResponse = null;
		try{
		//is = new FileInputStream("findInventory_output.xml");
		//doc = XMLUtil.getDocument(is);
	
		 YFCElement yfcinxmlElem = YFCDocument.getDocumentFor(doc).getDocumentElement();
		
		 /*Logic for creating shipment groups for the TMS call*/
		Map<String, ArrayList<YFCElement>>hmShipmentGroup = getShipmentGroup(yfcinxmlElem);

	
	     /* Forming response message for SalesForce
		  */
		if(null != hmShipmentGroup ) {
	   docSalesForceResponse = prepareInputforSalesForceResponse(env,doc,hmShipmentGroup,false);
	   log.verbose("Inside fulfillmentOptionsGrouping : prepareInputforSalesForceResponse output" + XMLUtil.getXMLString(docSalesForceResponse));
	  
	   YFCElement yfceleShipmentGroup = YFCDocument.getDocumentFor(docSalesForceResponse).getDocumentElement();
	   
	   dTMRequest = prepareInputForTMS(yfceleShipmentGroup); 
	   log.verbose("Inside fulfillmentOptionsGrouping : prepareInputForTMS output" + XMLUtil.getXMLString(dTMRequest));
	   /*
	    * Make a call to TMS only when we have shipments present in the 
	    */
	   if(dTMRequest.getDocumentElement().getChildNodes().getLength() > 0) {
		   docTMSWebserviceResponse =  callTMSWebservice(env,dTMRequest);
		   dTMResponse = processTMSWebserviceCallResponse(docSalesForceResponse,docTMSWebserviceResponse);
	   }
	   YFCElement yfceleShipmentGroupValidation =null;
		if(dTMResponse!=null)  
		{
	   yfceleShipmentGroupValidation = YFCDocument.getDocumentFor(dTMResponse).getDocumentElement();; 
		} else {
			yfceleShipmentGroupValidation = YFCDocument.getDocumentFor(docSalesForceResponse).getDocumentElement();; 
		}
	   boolean isShimentModeLTL = false;
	   String sSellerOrganizationCode = yfcinxmlElem.getAttribute(CommonConstants.ORGANIZATION_CODE);
	   double dLTLWeight = 0.0;
	   double dParcelWeight = 0.0;
	   double dLTLTotal = 0.0;
	   double dParcelTotal = 0.0;
	   if(sSellerOrganizationCode.equals(CommonConstants.SELLER_ORGANIZATION_PDC) || sSellerOrganizationCode.equals(CommonConstants.SELLER_ORGANIZATION_FLEETPRIDE_DIRECT)) {
		   Document docCommonCodeOutput = getCommonCodeForValidation(env);
		 //  Document docCommonCodeOutput = getCommonCodeForValidation();
		   Element eleLTLWeight = XMLUtil.getElementByXPath(docCommonCodeOutput,
					"/CommonCodeList/CommonCode[@CodeValue = '"+ CommonConstants.COMMON_CODE_WEIGHT_LTL+"']");
		 //  System.out.println("++++++++++eleLTLWeight++++++++" + eleLTLWeight.getAttribute(CommonConstants.CODE_SHORT_DESCRIPTION));
		   if((eleLTLWeight!=null)){
		   dLTLWeight =  Double.parseDouble(eleLTLWeight.getAttribute(CommonConstants.CODE_SHORT_DESCRIPTION));
		   }
		   
		   Element eleParcelWeight = XMLUtil.getElementByXPath(docCommonCodeOutput,
					"/CommonCodeList/CommonCode[@CodeValue = '"+ CommonConstants.COMMON_CODE_WEIGHT_PARCEL+"']");
		   if(eleParcelWeight!=null){
		   dParcelWeight =  Double.parseDouble(eleParcelWeight.getAttribute(CommonConstants.CODE_SHORT_DESCRIPTION));
		   }
		   Element eleLTLTotal = XMLUtil.getElementByXPath(docCommonCodeOutput,
					"/CommonCodeList/CommonCode[@CodeValue = '"+ CommonConstants.COMMON_CODE_TOTAL_LTL+"']");
		   if(eleLTLTotal!=null){
		    dLTLTotal = Double.parseDouble(eleLTLTotal.getAttribute(CommonConstants.CODE_SHORT_DESCRIPTION));
		   }
		   Element eleParcelTotal = XMLUtil.getElementByXPath(docCommonCodeOutput,
					"/CommonCodeList/CommonCode[@CodeValue = '"+ CommonConstants.COMMON_CODE_TOTAL_PARCEL+"']");
		   if(eleParcelTotal!=null){
		   dParcelTotal =  Double.parseDouble(eleParcelTotal.getAttribute(CommonConstants.CODE_SHORT_DESCRIPTION));
		   }
		   boolean bParcelEligible = true;
		   boolean bParcelShippingAllowed = true;
		   
		    YFCNodeList<YFCElement> nlPromiseShipmentV = yfceleShipmentGroupValidation.getElementsByTagName(CommonConstants.PROMISE_SHIPMENT);
		    for(YFCElement elePromiseShipmentV : nlPromiseShipmentV) {	
		    	  if((CommonConstants.DELIVERY_METHOD_SHP).equals(elePromiseShipmentV.getAttribute(CommonConstants.DELIVERY_METHOD))){
		    	YFCElement elePromiseShipmentLinesV = elePromiseShipmentV.getChildElement(CommonConstants.PROMISE_SHIPMENT_LINES);
		    	YFCNodeList<YFCElement> enPromiseShipmentLineV = elePromiseShipmentLinesV.getElementsByTagName(CommonConstants.PROMISE_SHIPMENT_LINE);
		    	   for(YFCElement elePromiseShipmentLineV : enPromiseShipmentLineV) {
		    		 
		    		   double weight =  elePromiseShipmentLineV.getDoubleAttribute(CommonConstants.ITEM_UNIT_WEIGHT);
		    		   double quantity = elePromiseShipmentLineV.getDoubleAttribute(CommonConstants.AVAILABLE_QUANTITY);
		    		   dItemWeight = dItemWeight + (weight * quantity);
					   double unitCost =  elePromiseShipmentLineV.getDoubleAttribute(CommonConstants.ITEM_UNIT_COST);
					   dItemUnitCost = dItemUnitCost + (unitCost * quantity);	
		    		 String sShipmentMode = elePromiseShipmentLineV.getAttribute(CommonConstants.SHIPMENT_MODE);
		    		/* if(sShipmentMode.equals(CommonConstants.SHIPMENT_MODE_LTL)) {
		    			 isShimentModeLTL = true;
		    		 }*/
		    		 if(sShipmentMode != ""){
						  if((!(sShipmentMode.equals(CommonConstants.SHIPMENT_MODE_PARCEL))) && (bParcelEligible)){
						  //isParcelIneligible = "Y";
							  
						  bParcelEligible = false;
						  }
					  }//else if(((elePromiseShipmentLineV.getAttribute(CommonConstants.IS_PARCEL_SHIPPING_ALLOWED)).equals("N")) && (bParcelShippingAllowed)){
						 // isParcelIneligible = "Y";
						  //bParcelShippingAllowed = false;
					//  }
		    	  }
		    	 
		    	    
		    	 //  if((((!bParcelEligible)||(!bParcelShippingAllowed)) && (dItemUnitCost > dLTLTotal) && (dItemWeight > dLTLWeight)) ||
		    	//		  ((bParcelEligible || (bParcelShippingAllowed))&& (dItemUnitCost > dParcelTotal) && (dItemWeight > dParcelWeight))){
		    	   if(((!bParcelEligible) && (dItemUnitCost > dLTLTotal) && (dItemWeight > dLTLWeight)) ||
		   		    		  ((bParcelEligible)&& (dItemUnitCost > dParcelTotal) && (dItemWeight > dParcelWeight))){
		    		   elePromiseShipmentV.setAttribute(CommonConstants.DISPLAY_FREIGHT_CHARGES,CommonConstants.FALSE);
		    	   } else {
		    		   elePromiseShipmentV.setAttribute(CommonConstants.DISPLAY_FREIGHT_CHARGES,CommonConstants.TRUE);
		    	   }
		    		   
		    	    dItemWeight = 0.0;
		    	    dItemUnitCost = 0.0;
		    	  }
		    }
	   }
	
	   log.endTimer("fulfillmentOptionsGrouping");
		}
		}
		catch (Exception e) {
			 log.verbose("Inside fulfillmentOptionsGrouping : exception", e);
			e.printStackTrace();
			log.verbose("InSide Exception",e);
			log.error(e.getMessage());
			throw new YFSException("logger:Inside fulfillmentOptionsGrouping");
		}
		
		if(dTMResponse!= null) {
			 log.verbose("Inside fulfillmentOptionsGrouping : end" + XMLUtil.getXMLString(dTMResponse));	
			return dTMResponse;
		}else {
			log.verbose("Inside fulfillmentOptionsGrouping : end" + XMLUtil.getXMLString(doc));	
			return doc;
		}
		
	}
	
	/*public static void main(String []args) throws Exception {
		FulfillmentOptionsValidations fOV = new FulfillmentOptionsValidations();
		
		fOV.fulfillmentOptionsGrouping();
		
	}*/
}
